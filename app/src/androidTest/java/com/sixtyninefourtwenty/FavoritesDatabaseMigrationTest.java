package com.sixtyninefourtwenty;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import androidx.room.testing.MigrationTestHelper;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.sixtyninefourtwenty.bcud.objects.favorites.FavoritesDatabase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(AndroidJUnit4.class)
public class FavoritesDatabaseMigrationTest {

    private static final String TEST_DB = "migration-test";

    @Rule
    public MigrationTestHelper helper;

    @Before
    public void before() {
        helper = new MigrationTestHelper(
                InstrumentationRegistry.getInstrumentation(),
                FavoritesDatabase.class
        );
    }

    @Test
    public void migrate1To2() throws IOException {
        try (final var db = helper.createDatabase(TEST_DB, 1)) {
            db.execSQL("INSERT INTO favorites VALUES (?)", new Object[]{1});
            db.execSQL("INSERT INTO favorite_reasons VALUES (?, ?, ?)", new Object[]{0, 1, "foo"});
        }
        try (final var db = helper.runMigrationsAndValidate(TEST_DB, 2, true, FavoritesDatabase.MIGRATION_1_2)) {
            try (final var cursor = db.query("SELECT importance FROM favorite_reasons WHERE uid = ?", new Object[]{0})) {
                final var next = cursor.moveToNext();
                assertTrue(next);
                assertEquals(0F, cursor.getFloat(0), 0);
            }
        }
    }

}
