package com.sixtyninefourtwenty;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import androidx.test.core.app.ActivityScenario;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Uninterruptibles;
import com.sixtyninefourtwenty.bcud.ui.activities.PhotoEditorActivity;
import com.sixtyninefourtwenty.bcud.utils.RemovableListenerListenableFutureWrapper;

import org.awaitility.core.ConditionTimeoutException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@RunWith(AndroidJUnit4.class)
public class RemovableListenerListenableFutureWrapperTest {

    private ListeningExecutorService executorService;

    private RemovableListenerListenableFutureWrapper<Void> createFuture() {
        return new RemovableListenerListenableFutureWrapper<>(
                executorService.submit(() -> {
                    Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
                    return null;
                })
        );
    }

    @Test
    public void test() throws Exception {
        final var resultThatShouldBeTriggered = new AtomicBoolean(false);
        final var resultThatShouldNotBeTriggered = new AtomicBoolean(false);
        final var future = createFuture();
        future.addListener(ignored -> resultThatShouldNotBeTriggered.set(true), MoreExecutors.directExecutor());
        future.clearListeners();
        assertEquals(0, future.numberOfListeners());
        future.addListener(ignored -> resultThatShouldBeTriggered.set(true), MoreExecutors.directExecutor());
        assertEquals(1, future.numberOfListeners());

        future.get();
        assertThrows("resultThatShouldNotBeTriggered is not supposed to be triggered",
                ConditionTimeoutException.class,
                () -> await().atMost(Duration.ofSeconds(1))
                        .until(resultThatShouldNotBeTriggered::get));
        await().atMost(Duration.ofSeconds(1))
                .until(resultThatShouldBeTriggered::get);
    }

    @Test
    public void containerTest() throws Exception {
        final var resultThatShouldNotBeTriggered = new AtomicBoolean(false);
        final var future = createFuture();
        future.addListener(ignored -> resultThatShouldNotBeTriggered.set(true), MoreExecutors.directExecutor());
        final var containerRef = new AtomicReference<RemovableListenerListenableFutureWrapper.Container>();
        try (final var scenario = ActivityScenario.launch(PhotoEditorActivity.class)) {
            scenario.onActivity(activity -> {
                final var container = new RemovableListenerListenableFutureWrapper.Container(activity);
                container.add(future);
                containerRef.set(container);
                assertTrue(container.hasListeners());
            });
        }

        assertFalse(containerRef.get().hasListeners());

        future.get();

        assertThrows("resultThatShouldNotBeTriggered is not supposed to be triggered",
                ConditionTimeoutException.class,
                () -> await().atMost(Duration.ofSeconds(1))
                        .until(resultThatShouldNotBeTriggered::get));
    }

    @Before
    public void setUp() {
        executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    }

    @After
    public void tearDown() {
        executorService.shutdown();
    }

}
