package com.sixtyninefourtwenty;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static java.util.Objects.requireNonNull;

import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoritesDatabase;
import com.sixtyninefourtwenty.bcud.repository.FavoritesDataRepository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

@LargeTest
@RunWith(AndroidJUnit4.class)
class FavoritesDataRepositoryTest {

    private ListeningExecutorService executorService;
    private FavoritesDataRepository repository;

    @BeforeEach
    void setup() {
        executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        final var context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        final var db = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), FavoritesDatabase.class).allowMainThreadQueries().build();
        repository = new FavoritesDataRepository(db, executorService);
    }

    @AfterEach
    void tearDown() {
        executorService.shutdown();
    }

    @Test
    void addItemWithNoReasons() throws Exception {
        final var item = new FavoriteItem(24);
        repository.addFavorites(Collections.singleton(item)).get();
        final var data = repository.getAllFavoritesAndReasonsSnapshot().get();
        assertTrue(data.containsKey(item));
        final var associatedReasons = data.get(item);
        assertNotNull(associatedReasons);
        assertTrue(associatedReasons.isEmpty());
    }

    @Test
    void addReasonWithoutItem() {
        final var reason = new FavoriteReason(24, "", 0);
        assertThrows(SQLiteConstraintException.class, () -> {
            try {
                repository.addFavoriteReasons(List.of(reason)).get();
            } catch (ExecutionException e) {
                throw requireNonNull(e.getCause());
            }
        });
    }

    @Test
    void addItemAndReason() throws Exception {
        final var item = new FavoriteItem(24);
        repository.addFavorites(List.of(item)).get();
        final var reason = new FavoriteReason(1, 24, "", 0);
        repository.addFavoriteReasons(List.of(reason)).get();
        final var data = repository.getAllFavoritesAndReasonsSnapshot().get();
        final var foundReasons = data.get(item);
        assertNotNull(foundReasons);
        assertTrue(foundReasons.contains(reason));
        assertEquals(1, foundReasons.size());
    }

    @Test
    void addItemAndReasonAndDeleteItemThenReasonsAreDeletedToo() throws Exception {
        final var item = new FavoriteItem(24);
        repository.addFavorites(List.of(item)).get();
        final var reason = new FavoriteReason(1, 24, "", 0);
        repository.addFavoriteReasons(List.of(reason)).get();
        final var reasonsAfterAddingItem = repository.getReasonsForFavoriteSnapshot(24).get();
        assertEquals(1, reasonsAfterAddingItem.size());
        repository.deleteFavorites(List.of(item));
        final var reasonsAfterDeleteItem = repository.getReasonsForFavoriteSnapshot(24).get();
        assertTrue(reasonsAfterDeleteItem.isEmpty());
    }

}
