package com.sixtyninefourtwenty.bcud.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

@NonNullTypesByDefault
public final class DatasetPreferences {

    private final SharedPreferences preferences;

    public DatasetPreferences(Context context) {
        preferences = context.getSharedPreferences("dataset", Context.MODE_PRIVATE);
    }

    @Nullable
    private Uri getUri(String key) {
        final var uriString = preferences.getString(key, null);
        return uriString != null ? Uri.parse(uriString) : null;
    }

    private void setUri(String key, @Nullable Uri uri) {
        preferences.edit()
                .putString(key, uri != null ? uri.toString() : null)
                .apply();
    }

    @Nullable
    public Uri getUnitTFMaterialData() {
        return getUri("unit_tf_material_data");
    }

    public void setUnitTFMaterialData(@Nullable Uri unitTFMaterialData) {
        setUri("unit_tf_material_data", unitTFMaterialData);
    }

    @Nullable
    public Uri getUnitTalentData() {
        return getUri("unit_talent_data");
    }

    public void setUnitTalentData(@Nullable Uri unitTalentData) {
        setUri("unit_talent_data", unitTalentData);
    }

    @Nullable
    public Uri getUnitHPData() {
        return getUri("unit_hp_data");
    }

    public void setUnitHPData(@Nullable Uri unitHPData) {
        setUri("unit_hp_data", unitHPData);
    }

    @Nullable
    public Uri getUnitEEPData() {
        return getUri("unit_eep_data");
    }

    public void setUnitEEPData(@Nullable Uri unitEEPData) {
        setUri("unit_eep_data", unitEEPData);
    }

    @Nullable
    public Uri getUnitBaseData() {
        return getUri("unit_base_data");
    }

    public void setUnitBaseData(@Nullable Uri unitBaseData) {
        setUri("unit_base_data", unitBaseData);
    }

}
