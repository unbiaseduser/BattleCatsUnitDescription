package com.sixtyninefourtwenty.bcud.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link ViewModelStoreOwner} scoped to multiple {@link Fragment}s. This scope is cancelled if <em>any</em>
 * fragment gets permanently destroyed. This is useful for using a single {@link ViewModel} class for
 * communication between multiple fragments displayed in one screen, across multiple screens without
 * relying on singletons (<i>cough</i> your activity).
 */
@NonNullTypesByDefault
public final class MultiFragmentViewModelStoreOwner implements ViewModelStoreOwner, HasDefaultViewModelProviderFactory {

    private static final Map<String, ViewModelStore> stores = new HashMap<>();
    private static final Table<String, String, ClearStatusViewModel> table = HashBasedTable.create();

    private final String tag;
    private final ViewModelProvider.Factory delegate;

    /**
     * @param tag Unique tag identifying a group of fragments, all of which must use the same tag
     *            to instantiate this class.
     */
    public MultiFragmentViewModelStoreOwner(String tag, Fragment fragment) {
        this.tag = tag;
        delegate = fragment.getDefaultViewModelProviderFactory();
        // Get fragment class as unique id to handle cases where one fragment creates multiple instances of this class
        final var fragmentId = fragment.getClass().getName();
        if (!table.contains(tag, fragmentId)) {
            final var vm = new ViewModelProvider(fragment, ViewModelProvider.Factory.from(ClearStatusViewModel.INITIALIZER)).get(ClearStatusViewModel.class);
            vm.setOnClearedListener(() -> {
                getViewModelStore().clear();
                stores.remove(tag);
                final var existingRowsByTag = List.copyOf(table.row(tag).entrySet()); // Make a copy of the row to avoid ConcurrentModificationException
                for (final var row : existingRowsByTag) {
                    table.remove(tag, row.getKey());
                }
            });
            table.put(tag, fragmentId, vm);
        }
    }

    @NonNull
    @Override
    public ViewModelStore getViewModelStore() {
        return stores.computeIfAbsent(tag, ignored -> new ViewModelStore());
    }

    @NonNull
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        return delegate;
    }

    private static final class ClearStatusViewModel extends ViewModel {

        private boolean isCleared = false;
        @Nullable
        private Runnable onClearedListener;

        public void setOnClearedListener(Runnable onClearedListener) {
            this.onClearedListener = onClearedListener;
            if (isCleared) {
                onClearedListener.run();
            }
        }

        @Override
        protected void onCleared() {
            isCleared = true;
            if (onClearedListener != null) {
                onClearedListener.run();
            }
        }

        /**
         * Initializer to keep this class private
         */
        public static final ViewModelInitializer<ClearStatusViewModel> INITIALIZER = new ViewModelInitializer<>(ClearStatusViewModel.class, creationExtras -> new ClearStatusViewModel());

    }

}
