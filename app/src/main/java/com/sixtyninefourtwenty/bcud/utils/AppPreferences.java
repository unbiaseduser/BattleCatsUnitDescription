package com.sixtyninefourtwenty.bcud.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferencesSupplier;
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferencesSuppliers;

import kotlin.collections.CollectionsKt;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Delegate;

@NonNullTypesByDefault
public final class AppPreferences implements ThemingPreferencesSupplier {

    public static AppPreferences get(Context context) {
        return MyApplication.get(context).getPrefs();
    }

    public static void initializePreferences(Context context) {
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
    }

    private final SharedPreferences preferences;

    @Delegate
    private final ThemingPreferencesSupplier delegate;

    public AppPreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        delegate = ThemingPreferencesSuppliers.create(preferences, context);
    }

    @AllArgsConstructor
    @Getter
    public enum ListViewMode {
        LIST("list"),
        GRID("grid");
        private final String value;
        public static final ImmutableList<ListViewMode> VALUES = ImmutableList.copyOf(values());
    }

    public ListViewMode getListViewMode() {
        return CollectionsKt.first(ListViewMode.VALUES, m -> m.value.equals(preferences.getString("list_mode", ListViewMode.GRID.value)));
    }

    public void setListViewMode(ListViewMode value) {
        preferences.edit().putString("list_mode", value.value).apply();
    }

    @AllArgsConstructor
    @Getter
    public enum AdventViewMode {
        TABS("tabs"),
        TEXT("texts");
        private final String value;
        public static final ImmutableList<AdventViewMode> VALUES = ImmutableList.copyOf(values());
    }

    public AdventViewMode getAdventViewMode() {
        return CollectionsKt.first(AdventViewMode.VALUES, m -> m.value.equals(preferences.getString("advent_view_mode", AdventViewMode.TABS.value)));
    }

    public void setAdventViewMode(AdventViewMode value) {
        preferences.edit().putString("advent_view_mode", value.value).apply();
    }

    public boolean getUseToc() {
        return preferences.getBoolean("use_toc", true);
    }

    public boolean getFirstLaunch() {
        return preferences.getBoolean("first_launch", true);
    }

    public void setFirstLaunch(boolean value) {
        preferences.edit().putBoolean("first_launch", value).apply();
    }

    public boolean isThemingPreferencesMigrated() {
        return preferences.getBoolean("theming_preferences_migrated", false);
    }

    public void setThemingPreferencesMigrated(boolean value) {
        preferences.edit().putBoolean("theming_preferences_migrated", value).apply();
    }

    @SuppressWarnings("unused")
    public boolean resetSetting(String key) {
        if (preferences.contains(key)) {
            preferences.edit().remove(key).apply();
            return true;
        }
        return false;
    }


}
