package com.sixtyninefourtwenty.bcud.utils;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.summingInt;

import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.sixtyninefourtwenty.common.utils.FutureCallbackIgnoreExceptions;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

import io.vavr.Tuple;
import io.vavr.Tuple2;

public final class RemovableListenerListenableFutureWrapper<V> {

    private final Set<Tuple2<FutureCallback<V>, Executor>> listeners = Collections.synchronizedSet(new HashSet<>());
    private final ListenableFuture<V> wrapped;

    public RemovableListenerListenableFutureWrapper(ListenableFuture<V> wrapped) {
        this.wrapped = wrapped;
        wrapped.addListener(() -> listeners.forEach(tuple -> tuple._2.execute(() -> {
            try {
                tuple._1.onSuccess(wrapped.get());
            } catch (ExecutionException e) {
                tuple._1.onFailure(requireNonNull(e.getCause()));
            } catch (Throwable t) {
                tuple._1.onFailure(t);
            }
        })), MoreExecutors.directExecutor());
    }

    /**
     * Call through to {@link Future#get()}.
     */
    public V get() throws ExecutionException, InterruptedException {
        return wrapped.get();
    }

    public void addListener(@NonNull FutureCallback<V> listener, @NonNull Executor exec) {
        listeners.add(Tuple.of(listener, exec));
    }

    public void addListener(@NonNull FutureCallbackIgnoreExceptions<V> listener, @NonNull Executor exec) {
        addListener((FutureCallback<V>) listener, exec);
    }

    public void clearListeners() {
        listeners.clear();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    public int numberOfListeners() {
        return listeners.size();
    }

    public static final class Container {

        private final Set<RemovableListenerListenableFutureWrapper<?>> futures = Collections.synchronizedSet(new HashSet<>());

        public Container(Lifecycle lifecycle) {
            final var state = lifecycle.getCurrentState();
            if (state != Lifecycle.State.DESTROYED && state.isAtLeast(Lifecycle.State.INITIALIZED)) {
                lifecycle.addObserver(new LifecycleEventObserver() {
                    @Override
                    public void onStateChanged(@NonNull LifecycleOwner lifecycleOwner, Lifecycle.@NonNull Event event) {
                        if (lifecycle.getCurrentState().compareTo(Lifecycle.State.DESTROYED) <= 0) {
                            lifecycle.removeObserver(this);
                            clear();
                        }
                    }
                });
            }
        }

        public Container(LifecycleOwner lifecycleOwner) {
            this(lifecycleOwner.getLifecycle());
        }

        public <T> RemovableListenerListenableFutureWrapper<T> add(ListenableFuture<T> future) {
            return add(new RemovableListenerListenableFutureWrapper<>(future));
        }

        public <T> RemovableListenerListenableFutureWrapper<T> add(RemovableListenerListenableFutureWrapper<T> future) {
            futures.add(future);
            return future;
        }

        public void clear() {
            futures.forEach(RemovableListenerListenableFutureWrapper::clearListeners);
            futures.clear();
        }

        @VisibleForTesting(otherwise = VisibleForTesting.NONE)
        public boolean hasListeners() {
            return futures.stream()
                    .collect(summingInt(RemovableListenerListenableFutureWrapper::numberOfListeners)) > 0;
        }

    }

}
