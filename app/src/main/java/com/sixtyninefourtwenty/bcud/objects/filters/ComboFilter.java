package com.sixtyninefourtwenty.bcud.objects.filters;

import static java.util.Objects.requireNonNull;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.core.os.ParcelCompat;

import com.google.common.collect.ImmutableSet;
import com.sixtyninefourtwenty.bcud.objects.Combo;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.repository.helper.ComboNameSupplier;
import com.sixtyninefourtwenty.bcud.utils.Utils;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.EnumSet;
import java.util.function.Predicate;

import kotlin.collections.CollectionsKt;
import kotlin.text.StringsKt;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NonNullTypesByDefault
@With
public class ComboFilter implements Parcelable {

    @Value
    @AllArgsConstructor
    @With
    public static class UnitFilterArgs implements Parcelable {

        @Nullable
        Iterable<Unit> units;
        Type type;

        private UnitFilterArgs(Parcel in) {
            units = in.createTypedArrayList(Unit.CREATOR);
            type = requireNonNull(ParcelCompat.readSerializable(in, null, Type.class));
        }

        public static final Creator<UnitFilterArgs> CREATOR = new Creator<>() {
            @Override
            public UnitFilterArgs createFromParcel(Parcel in) {
                return new UnitFilterArgs(in);
            }

            @Override
            public UnitFilterArgs[] newArray(int size) {
                return new UnitFilterArgs[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags) {
            dest.writeTypedList(units != null ? CollectionsKt.toList(units) : null);
            dest.writeSerializable(type);
        }

        public enum Type {
            ANY, ALL;
        }
    }

    public static final ComboFilter EMPTY = new ComboFilter(null, null, null);

    public static ComboFilter of(
            @Nullable String nameQuery,
            @Nullable Collection<Combo.Type> types,
            @Nullable UnitFilterArgs unitFilterArgs
    ) {
        if (nameQuery == null && types == null && unitFilterArgs == null) {
            return EMPTY;
        }
        return new ComboFilter(nameQuery, !Utils.isNullOrEmpty(types) ? Utils.copyOfOrThis(types) : null, unitFilterArgs);
    }

    @Nullable
    String nameQuery;
    @Nullable
    EnumSet<Combo.Type> types;
    @Nullable
    UnitFilterArgs unitFilterArgs;

    public ComboFilter withUnits(@Nullable Iterable<Unit> units) {
        if (unitFilterArgs != null) {
            return withUnitFilterArgs(unitFilterArgs.withUnits(units));
        }
        return withUnitFilterArgs(null);
    }

    public @Nullable ImmutableSet<Combo.Type> getTypes() {
        return types != null ? ImmutableSet.copyOf(types) : null;
    }

    public UnitFilterArgs.@Nullable Type getUnitFilterType() {
        return unitFilterArgs != null ? unitFilterArgs.getType() : null;
    }

    public Predicate<Combo> asPredicate(ComboNameSupplier comboNameSupplier) {
        return combo -> test(combo, comboNameSupplier);
    }

    public boolean test(Combo combo, ComboNameSupplier comboNameSupplier) {
        if (nameQuery != null) {
            final var passesNameQuery = StringsKt.contains(combo.getName(comboNameSupplier), nameQuery, true);
            if (!passesNameQuery) {
                return false;
            }
        }
        if (!Utils.isNullOrEmpty(types)) {
            var passesTypeQuery = false;
            for (final var type : types) {
                if (combo.getType() == type) {
                    passesTypeQuery = true;
                    break;
                }
            }
            if (!passesTypeQuery) {
                return false;
            }
        }
        if (unitFilterArgs != null && unitFilterArgs.getUnits() != null) {
            var passesUnitQuery = false;
            final var units = unitFilterArgs.getUnits();
            switch (unitFilterArgs.getType()) {
                case ANY -> {
                    for (final var unit : units) {
                        if (combo.getUnits().contains(unit)) {
                            passesUnitQuery = true;
                            break;
                        }
                    }
                }
                case ALL -> passesUnitQuery = Utils.toCollectionOrThis(units).containsAll(combo.getUnits());
            }
            if (!passesUnitQuery) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private ComboFilter(Parcel in) {
        nameQuery = in.readString();
        types = (EnumSet<Combo.Type>) ParcelCompat.readSerializable(in, null, EnumSet.class);
        unitFilterArgs = ParcelCompat.readParcelable(in, null, UnitFilterArgs.class);
    }

    public static final Creator<ComboFilter> CREATOR = new Creator<>() {
        @Override
        public ComboFilter createFromParcel(Parcel in) {
            return new ComboFilter(in);
        }

        @Override
        public ComboFilter[] newArray(int size) {
            return new ComboFilter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(nameQuery);
        dest.writeSerializable(types);
        dest.writeParcelable(unitFilterArgs, flags);
    }
}
