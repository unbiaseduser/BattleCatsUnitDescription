package com.sixtyninefourtwenty.bcud.objects.favorites;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

@Database(entities = {FavoriteItem.class, FavoriteReason.class}, version = 2)
@NonNullTypesByDefault
public abstract class FavoritesDatabase extends RoomDatabase {
    public abstract FavoritesDao favoritesDao();

    public static FavoritesDatabase create(Context context) {
        return Room.databaseBuilder(context, FavoritesDatabase.class, "favorites")
                .addMigrations(MIGRATION_1_2)
                .build();
    }

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.execSQL("ALTER TABLE favorite_reasons ADD COLUMN importance FLOAT NOT NULL DEFAULT 0");
        }
    };
}
