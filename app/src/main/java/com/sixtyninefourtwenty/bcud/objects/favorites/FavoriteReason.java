package com.sixtyninefourtwenty.bcud.objects.favorites;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.FloatRange;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.sixtyninefourtwenty.common.interfaces.JsonSerializer;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

@Entity(tableName = "favorite_reasons",
        indices = {
                @Index(value = "unit_id")
        },
        foreignKeys = {
                @ForeignKey(
                        entity = FavoriteItem.class,
                        parentColumns = {"id"},
                        childColumns = {"unit_id"},
                        onDelete = ForeignKey.CASCADE
                )
        })
public final class FavoriteReason implements Parcelable {

    public FavoriteReason(int uid, int unitId, String reason, float importance) {
        this.uid = uid;
        this.unitId = unitId;
        this.reason = reason;
        this.importance = importance;
    }

    public int getUid() {
        return uid;
    }

    public int getUnitId() {
        return unitId;
    }

    public String getReason() {
        return reason;
    }

    public float getImportance() {
        return importance;
    }

    @PrimaryKey(autoGenerate = true)
    private final int uid;
    @ColumnInfo(name = "unit_id")
    private final int unitId;
    private final String reason;
    @FloatRange(from = 0.0, to = 10.0)
    private final float importance;

    @Ignore
    public FavoriteReason(int unitId, String reason, float importance) {
        this(0, unitId, reason, importance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FavoriteReason that)) return false;
        return uid == that.uid && unitId == that.unitId && Objects.equals(reason, that.reason) &&
                Float.compare(importance, that.importance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, unitId, reason, importance);
    }

    public static final JsonSerializer<FavoriteReason> SERIALIZER = new JsonSerializer<>() {

        @Override
        @NonNull
        public JSONObject toJson(FavoriteReason obj) throws JSONException {
            return new JSONObject()
                    .put("uid", obj.uid)
                    .put("unit_id", obj.unitId)
                    .put("reason", obj.reason)
                    .put("importance", obj.importance);
        }

        @Override
        @NonNull
        public FavoriteReason fromJson(JSONObject obj) throws JSONException {
            return new FavoriteReason(
                    obj.getInt("uid"),
                    obj.getInt("unit_id"),
                    obj.getString("reason"),
                    (float) obj.optDouble("importance", 0D)
            );
        }
    };

    private FavoriteReason(Parcel in) {
        uid = in.readInt();
        unitId = in.readInt();
        reason = in.readString();
        importance = in.readFloat();
    }

    public static final Creator<FavoriteReason> CREATOR = new Creator<>() {
        @Override
        public FavoriteReason createFromParcel(Parcel in) {
            return new FavoriteReason(in);
        }

        @Override
        public FavoriteReason[] newArray(int size) {
            return new FavoriteReason[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(uid);
        dest.writeInt(unitId);
        dest.writeString(reason);
        dest.writeFloat(importance);
    }
}
