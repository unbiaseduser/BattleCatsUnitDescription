package com.sixtyninefourtwenty.bcud.objects.filters;

import static java.util.Objects.requireNonNull;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.core.os.ParcelCompat;

import com.google.common.collect.ImmutableSet;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.bcud.utils.Utils;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.EnumSet;
import java.util.function.Predicate;

import kotlin.text.StringsKt;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import lombok.With;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NonNullTypesByDefault
@With
public class UnitFilter implements Parcelable {

    public static final UnitFilter EMPTY = new UnitFilter(null, null);

    public static UnitFilter of(
            @Nullable String nameQuery,
            @Nullable Collection<UnitBaseData.Type> types
    ) {
        if (nameQuery == null && types == null) {
            return EMPTY;
        }
        return new UnitFilter(nameQuery, !Utils.isNullOrEmpty(types) ? Utils.copyOfOrThis(types) : null);
    }

    @Nullable
    String nameQuery;
    @Nullable
    @Getter(AccessLevel.NONE)
    EnumSet<UnitBaseData.Type> types;

    public @Nullable ImmutableSet<UnitBaseData.Type> getTypes() {
        return types != null ? ImmutableSet.copyOf(types) : null;
    }

    public Predicate<Unit> asPredicate(UnitExplanationSupplier unitExplanationSupplier) {
        return unit -> test(unit, unitExplanationSupplier);
    }

    public boolean test(Unit unit, UnitExplanationSupplier unitExplanationSupplier) {
        if (nameQuery != null) {
            final var explanation = unit.getExplanation(unitExplanationSupplier);
            final var containsFirstTwoForms = StringsKt.contains(explanation.getFirstFormName(), nameQuery, true) ||
                    StringsKt.contains(explanation.getSecondFormName(), nameQuery, true);
            final var passesNameQuery = unit.hasTF(unitExplanationSupplier) ?
                    containsFirstTwoForms || StringsKt.contains(requireNonNull(explanation.getTrueFormName()), nameQuery, true) :
                    containsFirstTwoForms;
            if (!passesNameQuery) {
                return false;
            }
        }
        if (!Utils.isNullOrEmpty(types)) {
            final var passesTypeQuery = types.contains(unit.getType());
            if (!passesTypeQuery) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private UnitFilter(Parcel in) {
        nameQuery = in.readString();
        types = (EnumSet<UnitBaseData.Type>) ParcelCompat.readSerializable(in, null, EnumSet.class);
    }

    public static final Creator<UnitFilter> CREATOR = new Creator<>() {
        @Override
        public UnitFilter createFromParcel(Parcel in) {
            return new UnitFilter(in);
        }

        @Override
        public UnitFilter[] newArray(int size) {
            return new UnitFilter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(nameQuery);
        dest.writeSerializable(types);
    }
}
