package com.sixtyninefourtwenty.bcud.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.bcud.utils.AssetImageLoading;
import com.sixtyninefourtwenty.bcud.utils.Utils;
import com.sixtyninefourtwenty.common.utils.Dimensions;
import com.sixtyninefourtwenty.common.utils.UnitIconRatioImageView;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.ObjIntConsumer;

public final class UnitListAdapterGrid extends ListAdapter<Unit, UnitListAdapterGrid.UnitListGridViewHolder> {

    public static int getGridSpans(Context context) {
        return context.getResources().getInteger(R.integer.unit_list_grid_size);
    }

    public UnitListAdapterGrid(
            Consumer<Unit> onItemClickListener,
            BiConsumer<View, Unit> onItemLongClickListener,
            @Nullable UnitExplanationSupplier unitExplanationSupplier
    ) {
        super(Utils.UNIT_DIFFER);
        this.onItemClickListener = onItemClickListener;
        this.onItemLongClickListener = onItemLongClickListener;
        this.unitExplanationSupplier = unitExplanationSupplier;
    }

    public UnitListAdapterGrid(
            Consumer<Unit> onItemClickListener,
            BiConsumer<View, Unit> onItemLongClickListener
    ) {
        this(onItemClickListener, onItemLongClickListener, null);
    }

    private final Consumer<Unit> onItemClickListener;
    private final BiConsumer<View, Unit> onItemLongClickListener;
    @Nullable
    private final UnitExplanationSupplier unitExplanationSupplier;

    @NonNull
    @Override
    public UnitListGridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final var iv = new UnitIconRatioImageView(parent.getContext());
        final var params = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final var horizontalMarginDp = (int) Dimensions.pxToDp(iv.getContext().getResources(), 10);
        params.setMargins(horizontalMarginDp, 0, horizontalMarginDp, 0);
        iv.setLayoutParams(params);
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return new UnitListGridViewHolder(iv,
                pos -> onItemClickListener.accept(getItem(pos)),
                (view, pos) -> onItemLongClickListener.accept(view, getItem(pos)));
    }

    @Override
    public void onBindViewHolder(@NonNull UnitListGridViewHolder holder, int position) {
        final var unit = getItem(position);
        final var context = holder.icon.getContext();
        final var unitExplanationData = Objects.requireNonNullElseGet(
                unitExplanationSupplier,
                () -> MyApplication.get(context).getUnitExplanationData()
        );
        AssetImageLoading.loadAssetImage(holder.icon, unit.getLatestFormIconPath(unitExplanationData));
    }

    public static final class UnitListGridViewHolder extends RecyclerView.ViewHolder {
        private final ImageView icon;

        public UnitListGridViewHolder(ImageView icon, IntConsumer onItemClickListener, ObjIntConsumer<View> onItemLongClickListener) {
            super(icon);
            this.icon = icon;
            itemView.setOnClickListener(v -> onItemClickListener.accept(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(v -> {
                onItemLongClickListener.accept(v, getAbsoluteAdapterPosition());
                return true;
            });
        }
    }
}
