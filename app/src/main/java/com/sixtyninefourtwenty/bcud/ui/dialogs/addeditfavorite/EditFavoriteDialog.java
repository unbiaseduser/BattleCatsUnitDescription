package com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite;

import static java.util.Objects.requireNonNull;

import androidx.core.os.BundleCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavDirections;

import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.common.utils.Bundles;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.LazyThreadSafetyMode;

public final class EditFavoriteDialog extends AbstractAddEditFavoriteDialog {

    private static final String EDIT_FAVORITE_REQUEST_KEY = "edit_favorite_request";
    private static final String FAVORITE_ITEM_KEY = "fav_item";
    private static final String FAVORITE_REASONS_KEY = "fav_reasons";

    public static void registerCallback(
            FragmentActivity activity,
            LifecycleOwner lifecycleOwner,
            BiConsumer<FavoriteItem, List<FavoriteReason>> callback
    ) {
        activity.getSupportFragmentManager().setFragmentResultListener(
                EDIT_FAVORITE_REQUEST_KEY,
                lifecycleOwner,
                (requestKey, result) -> callback.accept(
                        requireNonNull(BundleCompat.getParcelable(result, FAVORITE_ITEM_KEY, FavoriteItem.class)),
                        requireNonNull(BundleCompat.getParcelableArrayList(result, FAVORITE_REASONS_KEY, FavoriteReason.class))
                )
        );
    }

    private final Lazy<EditFavoriteDialogArgs> args = LazyKt.lazy(LazyThreadSafetyMode.NONE, () -> EditFavoriteDialogArgs.fromBundle(requireArguments()));

    private EditFavoriteDialogArgs getArgs() {
        return args.getValue();
    }

    @Override
    protected int getTitle() {
        return R.string.edit;
    }

    @Override
    protected @NonNull Unit getUnit() {
        return getArgs().getUnit();
    }

    @Override
    protected @NonNull List<@NonNull FavoriteReason> getExistingReasons() {
        return List.of(getArgs().getReasons());
    }

    @Override
    protected @NonNull NavDirections getAddReasonDialogDirections(int unitId) {
        return EditFavoriteDialogDirections.showAddReasonDialog(unitId);
    }

    @Override
    protected @NonNull NavDirections getEditReasonDialogDirections(FavoriteReason reason) {
        return EditFavoriteDialogDirections.showEditReasonDialog(reason);
    }

    @Override
    protected void onFavoriteCreated(@NonNull FavoriteItem favoriteItem, @NonNull List<@NonNull FavoriteReason> favoriteReasons) {
        requireActivity().getSupportFragmentManager().setFragmentResult(
                EDIT_FAVORITE_REQUEST_KEY,
                Bundles.createBundle(bundle -> {
                    bundle.putParcelable(FAVORITE_ITEM_KEY, favoriteItem);
                    bundle.putParcelableArrayList(FAVORITE_REASONS_KEY, new ArrayList<>(favoriteReasons));
                })
        );
    }
}
