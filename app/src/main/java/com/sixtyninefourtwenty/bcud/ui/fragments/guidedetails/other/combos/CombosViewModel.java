package com.sixtyninefourtwenty.bcud.ui.fragments.guidedetails.other.combos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.Combo;
import com.sixtyninefourtwenty.bcud.objects.filters.ComboFilter;
import com.sixtyninefourtwenty.bcud.objects.filters.UnitFilter;
import com.sixtyninefourtwenty.bcud.repository.UnitData;
import com.sixtyninefourtwenty.bcud.repository.helper.ComboNameSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.utils.SavedStateHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;
import java.util.function.BiFunction;

@NonNullTypesByDefault
public final class CombosViewModel extends ViewModel {

    private static final String UNIT_FILTER_KEY = "unit_filter";
    private static final String COMBO_FILTER_KEY = "combo_filter";

    private final MediatorLiveData<ImmutableList<Combo>> combosAfterFilterLiveData;
    private final MutableLiveData<@Nullable ComboFilter> comboFilterLiveData;
    private final MutableLiveData<@Nullable UnitFilter> unitFilterLiveData;

    public CombosViewModel(
            SavedStateHandle savedStateHandle,
            UnitData data,
            ComboNameSupplier comboNameSupplier,
            UnitExplanationSupplier unitExplanationSupplier
    ) {
        comboFilterLiveData = SavedStateHandles.getNullableParcelableLiveData(savedStateHandle, COMBO_FILTER_KEY, null);
        unitFilterLiveData = SavedStateHandles.getNullableParcelableLiveData(savedStateHandle, UNIT_FILTER_KEY, null);
        combosAfterFilterLiveData = new MediatorLiveData<>(data.getAllCombos());
        final BiFunction<@Nullable ComboFilter, @Nullable UnitFilter, @Nullable ComboFilter> composeComboFilter = (comboFilter, unitFilter) -> {
            final ComboFilter finalFilter;
            if (comboFilter != null) {
                finalFilter = unitFilter != null ?
                        comboFilter.withUnits(data.filterUnits(List.of(unitFilter.asPredicate(unitExplanationSupplier)))) :
                        comboFilter.withUnits(null);
            } else {
                finalFilter = null;
            }
            return finalFilter;
        };
        combosAfterFilterLiveData.addSource(unitFilterLiveData, unitFilter -> {
            final var finalFilter = composeComboFilter.apply(comboFilterLiveData.getValue(), unitFilter);
            combosAfterFilterLiveData.setValue(data.filterCombos(
                    finalFilter != null ? List.of(finalFilter.asPredicate(comboNameSupplier)) : List.of()
            ));
        });
        combosAfterFilterLiveData.addSource(comboFilterLiveData, comboFilter -> {
            final var finalFilter = composeComboFilter.apply(comboFilter, unitFilterLiveData.getValue());
            combosAfterFilterLiveData.setValue(data.filterCombos(
                    finalFilter != null ? List.of(finalFilter.asPredicate(comboNameSupplier)) : List.of()
            ));
        });
    }

    public LiveData<ImmutableList<Combo>> getCombosLiveData() {
        return combosAfterFilterLiveData;
    }

    public @Nullable UnitFilter getUnitFilter() {
        return unitFilterLiveData.getValue();
    }

    public void setUnitFilter(@Nullable UnitFilter unitFilter) {
        unitFilterLiveData.setValue(unitFilter);
    }

    public @Nullable ComboFilter getComboFilter() {
        return comboFilterLiveData.getValue();
    }

    public void setComboFilter(@Nullable ComboFilter comboFilter) {
        comboFilterLiveData.setValue(comboFilter);
    }

    public void resetSearchFilterCombosState() {
        setComboFilter(null);
        setUnitFilter(null);
    }

    public static final ViewModelInitializer<CombosViewModel> INITIALIZER = new ViewModelInitializer<>(CombosViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new CombosViewModel(
                SavedStateHandleSupport.createSavedStateHandle(creationExtras),
                myApp.getUnitData(),
                myApp.getComboNameData(),
                myApp.getUnitExplanationData()
        );
    });

    public static CombosViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(CombosViewModel.class);
    }
}
