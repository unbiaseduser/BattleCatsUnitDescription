package com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority;

import static com.sixtyninefourtwenty.common.interfaces.MyMenuItemClickListener.voidReturning;
import static com.sixtyninefourtwenty.common.interfaces.SimpleAdapterViewOnItemSelectedListener.simplify;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.adapters.UnitListAdapterGrid;
import com.sixtyninefourtwenty.bcud.adapters.UnitListAdapterList;
import com.sixtyninefourtwenty.bcud.databinding.HpTpTabBinding;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite.AddFavoriteDialog;
import com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite.EditFavoriteDialog;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.root.HypermaxPriorityFragment;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.root.HypermaxPriorityFragmentDirections;
import com.sixtyninefourtwenty.bcud.utils.MultiFragmentViewModelStoreOwner;
import com.sixtyninefourtwenty.bcud.utils.RemovableListenerListenableFutureWrapper;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingFragment;
import com.sixtyninefourtwenty.bcud.viewmodels.AppSettingsViewModel;
import com.sixtyninefourtwenty.bcud.viewmodels.SearchViewModel;
import com.sixtyninefourtwenty.common.concurrent.FutureContainer;
import com.sixtyninefourtwenty.common.concurrent.LifecycleAwareFutureContainer;
import com.sixtyninefourtwenty.common.objects.Hypermax;
import com.sixtyninefourtwenty.common.utils.MoreFutures;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.function.BiConsumer;

public abstract class HypermaxPriorityTabFragment extends BaseViewBindingFragment<@NonNull HpTpTabBinding> {
    protected abstract Hypermax.UnitType getHypermaxPriorityType();

    private FutureContainer futureContainer;
    private RemovableListenerListenableFutureWrapper.Container futureWrapperContainer;

    @Override
    protected @NonNull HpTpTabBinding initBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return HpTpTabBinding.inflate(inflater, container, false);
    }

    @Override
    protected void setup(@NonNull HpTpTabBinding binding, @Nullable Bundle savedInstanceState) {
        futureWrapperContainer = new RemovableListenerListenableFutureWrapper.Container(getViewLifecycleOwner());
        futureContainer = new LifecycleAwareFutureContainer(getViewLifecycleOwner());
        final var searchModel = SearchViewModel.get(new MultiFragmentViewModelStoreOwner(HypermaxPriorityFragment.VIEWMODEL_TAG, this));
        final var settingsModel = AppSettingsViewModel.get(requireActivity());
        final var viewModel = HypermaxPriorityTabViewModel.get(this);
        final var spn = binding.spinner;
        spn.setOnItemSelectedListener(simplify((parent, view, position, id) -> {
            switch (position) {
                case 0 -> viewModel.setHypermaxPriorityType(getHypermaxPriorityType(), Hypermax.Priority.MAX);
                case 1 -> viewModel.setHypermaxPriorityType(getHypermaxPriorityType(), Hypermax.Priority.HIGH);
                case 2 -> viewModel.setHypermaxPriorityType(getHypermaxPriorityType(), Hypermax.Priority.MID);
                case 3 -> viewModel.setHypermaxPriorityType(getHypermaxPriorityType(), Hypermax.Priority.LOW);
                case 4 -> viewModel.setHypermaxPriorityType(getHypermaxPriorityType(), Hypermax.Priority.MIN);
            }
        }));
        spn.setAdapter(new ArrayAdapter<>(
                requireContext(),
                com.sixtyninefourtwenty.theming.R.layout.spinner_item_m3,
                requireContext().getResources().getStringArray(R.array.hypermax_priorities)
        ));
        final var rcv = binding.list;
        rcv.setHasFixedSize(true);
        final var decoration = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);

        final BiConsumer<View, Unit> onItemLongClick = (v, unit) -> {
            final var popupMenu = new PopupMenu(requireContext(), v);
            final var menu = popupMenu.getMenu();
            menu.add(R.string.add_to_favorites)
                    .setIcon(R.drawable.star)
                    .setOnMenuItemClickListener(voidReturning(item -> {
                        MoreFutures.addIgnoreExceptionsCallback(
                                futureContainer.addAndReturn(viewModel.findFavoriteByUnitId(unit.getId())),
                                existingItem -> {
                                    if (existingItem != null) {
                                        MoreFutures.addIgnoreExceptionsCallback(
                                                futureContainer.addAndReturn(viewModel.getReasonsForFavoriteSnapshot(unit.getId())),
                                                reasons -> navigate(HypermaxPriorityFragmentDirections.showEditFavoriteDialog(unit, reasons.toArray(new FavoriteReason[0]))),
                                                ContextCompat.getMainExecutor(requireContext())
                                        );
                                    } else {
                                        navigate(HypermaxPriorityFragmentDirections.showAddFavoriteDialog(unit));
                                    }
                                },
                                ContextCompat.getMainExecutor(requireContext())
                        );
                    }));
            popupMenu.setForceShowIcon(true);
            popupMenu.show();
        };

        final var adapterGridView = new UnitListAdapterGrid(unit -> navigate(HypermaxPriorityFragmentDirections.actionNavHypermaxPriorityToNavUdpUnitInfo(unit)),
                onItemLongClick);
        final var adapterListView = new UnitListAdapterList(unit -> navigate(HypermaxPriorityFragmentDirections.actionNavHypermaxPriorityToNavUdpUnitInfo(unit)),
                onItemLongClick);
        settingsModel.getListViewMode().observe(getViewLifecycleOwner(), mode -> {
            switch (mode) {
                case LIST -> {
                    rcv.setLayoutManager(new LinearLayoutManager(requireContext()));
                    rcv.setAdapter(adapterListView);
                    rcv.addItemDecoration(decoration);
                }
                case GRID -> {
                    rcv.setLayoutManager(new GridLayoutManager(requireContext(), UnitListAdapterGrid.getGridSpans(requireContext())));
                    rcv.setAdapter(adapterGridView);
                    rcv.removeItemDecoration(decoration);
                }
            }
        });
        searchModel.getQuery().observe(getViewLifecycleOwner(), query -> viewModel.setHypermaxPriorityQuery(getHypermaxPriorityType(), query));

        viewModel.getHypermaxPriorityLiveData(getHypermaxPriorityType()).observe(getViewLifecycleOwner(), list -> {
            adapterListView.submitList(list);
            adapterGridView.submitList(list);
        });

        AddFavoriteDialog.registerCallback(
                requireActivity(),
                getViewLifecycleOwner(),
                (favoriteItem, favoriteReasons) ->
                        futureWrapperContainer.add(viewModel.addFavorite(favoriteItem, favoriteReasons))
                                .addListener(ignored -> showToast(R.string.add_favorite_success),
                                        ContextCompat.getMainExecutor(requireContext()))
        );

        EditFavoriteDialog.registerCallback(
                requireActivity(),
                getViewLifecycleOwner(),
                (favoriteItem, favoriteReasons) ->
                        futureWrapperContainer.add(viewModel.addFavorite(favoriteItem, favoriteReasons))
                                .addListener(ignored -> showToast(R.string.update_favorite_success),
                                        ContextCompat.getMainExecutor(requireContext()))
        );
    }

}
