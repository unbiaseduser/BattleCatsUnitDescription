package com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.root;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.HypermaxPriorityTabRareFragment;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.HypermaxPriorityTabSpecialFragment;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.HypermaxPriorityTabSuperRareFragment;
import com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority.texts.HypermaxPriorityTabTextsFragment;
import com.sixtyninefourtwenty.bcud.utils.AppPreferences;
import com.sixtyninefourtwenty.bcud.utils.MultiFragmentViewModelStoreOwner;
import com.sixtyninefourtwenty.bcud.utils.fragments.TabLayoutViewPagerFragment;
import com.sixtyninefourtwenty.bcud.viewmodels.AppSettingsViewModel;
import com.sixtyninefourtwenty.bcud.viewmodels.SearchViewModel;
import com.sixtyninefourtwenty.common.utils.ListFragmentStateAdapter;
import com.sixtyninefourtwenty.common.utils.Menus;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;

import lombok.AllArgsConstructor;

public final class HypermaxPriorityFragment extends TabLayoutViewPagerFragment {

    public static final String VIEWMODEL_TAG = HypermaxPriorityFragment.class.getSimpleName();

    private AppSettingsViewModel appSettingsViewModel;

    @Override
    protected RecyclerView.Adapter<?> getPagerAdapter(@NonNull LayoutInflater inflater, ViewGroup container) {
        return new ListFragmentStateAdapter(requireActivity(),
                List.of(HypermaxPriorityTabTextsFragment::new,
                        HypermaxPriorityTabSpecialFragment::new,
                        HypermaxPriorityTabRareFragment::new,
                        HypermaxPriorityTabSuperRareFragment::new));
    }

    @Override
    protected void setupTabs(TabLayout.Tab tab, int pos) {
        switch (pos) {
            case 0 -> tab.setText(R.string.tab_advent_intro);
            case 1 -> tab.setText(R.string.tab_special);
            case 2 -> tab.setText(R.string.rares);
            case 3 -> tab.setText(R.string.super_rares);
        }
    }

    @Override
    protected void setup(@NonNull View view, @Nullable Bundle savedInstanceState) {
        appSettingsViewModel = AppSettingsViewModel.get(requireActivity());
        final var model = SearchViewModel.get(new MultiFragmentViewModelStoreOwner(VIEWMODEL_TAG, this));
        requireActivity().addMenuProvider(new HypermaxPriorityFragmentMenu(model), getViewLifecycleOwner());
    }

    @Override
    public void onPause() {
        super.onPause();
        appSettingsViewModel.persistListViewMode();
    }

    @AllArgsConstructor
    private final class HypermaxPriorityFragmentMenu implements MenuProvider {
        private final SearchViewModel searchViewModel;

        @Override
        public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
            menuInflater.inflate(R.menu.menu_udp_hypermax, menu);
            var search = menu.findItem(R.id.udp_hp_search);
            var searchView = Menus.requireActionView(search, SearchView.class);
            searchViewModel.setToSearchViewIfPresent(search);
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.setSubmitButtonEnabled(false);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    searchViewModel.setQuery(newText);
                    return true;
                }
            });
            searchView.setOnQueryTextFocusChangeListener((v, hasFocus) -> {
                if (!hasFocus) {
                    search.collapseActionView();
                }
            });
        }

        @Override
        public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
            int id = menuItem.getItemId();
            if (id == R.id.udp_hp_toggle_list_mode) {
                appSettingsViewModel.setListViewMode(appSettingsViewModel.getListViewMode().getValue() == AppPreferences.ListViewMode.LIST ? AppPreferences.ListViewMode.GRID : AppPreferences.ListViewMode.LIST);
                return true;
            } else if (id == R.id.udp_hp_go_to_website) {
                openWebsite("https://thanksfeanor.pythonanywhere.com/hypermaxpriority");
                return true;
            }
            return false;
        }
    }

}
