package com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.hypermaxpriority;

import static java.util.Objects.requireNonNull;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByName;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByType;
import com.sixtyninefourtwenty.bcud.repository.FavoritesDataRepository;
import com.sixtyninefourtwenty.bcud.repository.UnitData;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.concurrent.FutureContainer;
import com.sixtyninefourtwenty.common.concurrent.ViewModelFutureContainer;
import com.sixtyninefourtwenty.common.objects.Hypermax;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import io.vavr.Tuple;
import io.vavr.Tuple2;

@NonNullTypesByDefault
public final class HypermaxPriorityTabViewModel extends ViewModel {

    private static final String RARE_HP_QUERY_KEY = "rare_hp_query";
    private static final String RARE_HP_PRIORITY_KEY = "rare_hp_priority";
    private static final String SUPER_RARE_HP_QUERY_KEY = "super_rare_hp_query";
    private static final String SUPER_RARE_HP_PRIORITY_KEY = "super_rare_hp_priority";
    private static final String SPECIAL_HP_QUERY_KEY = "special_hp_query";
    private static final String SPECIAL_HP_PRIORITY_KEY = "special_hp_priority";

    public HypermaxPriorityTabViewModel(
            SavedStateHandle savedStateHandle,
            UnitData data,
            UnitExplanationSupplier explanationSupplier,
            FavoritesDataRepository repository
    ) {
        this.savedStateHandle = savedStateHandle;
        this.data = data;
        this.explanationSupplier = explanationSupplier;
        this.repository = repository;
        final var rareHypermaxPriorityQueryLiveData = savedStateHandle.getLiveData(RARE_HP_QUERY_KEY, "");
        final var rareHypermaxPriorityTypeLiveData = savedStateHandle.getLiveData(RARE_HP_PRIORITY_KEY, Hypermax.Priority.MAX);
        rareHypermaxPriorityIntermediate = new MediatorLiveData<>();
        rareHypermaxPriorityIntermediate.addSource(rareHypermaxPriorityQueryLiveData, query -> rareHypermaxPriorityIntermediate.setValue(Tuple.of(query, data.getHypermaxPriorityList(requireNonNull(rareHypermaxPriorityTypeLiveData.getValue()), Hypermax.UnitType.RARE))));
        rareHypermaxPriorityIntermediate.addSource(rareHypermaxPriorityTypeLiveData, type -> rareHypermaxPriorityIntermediate.setValue(Tuple.of(requireNonNull(rareHypermaxPriorityQueryLiveData.getValue()), data.getHypermaxPriorityList(type, Hypermax.UnitType.RARE))));
        rareHypermaxPriority = Transformations.map(rareHypermaxPriorityIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var superRareHypermaxPriorityQueryLiveData = savedStateHandle.getLiveData(SUPER_RARE_HP_QUERY_KEY, "");
        final var superRareHypermaxPriorityTypeLiveData = savedStateHandle.getLiveData(SUPER_RARE_HP_PRIORITY_KEY, Hypermax.Priority.MAX);
        superRareHypermaxPriorityIntermediate = new MediatorLiveData<>();
        superRareHypermaxPriorityIntermediate.addSource(superRareHypermaxPriorityQueryLiveData, query -> superRareHypermaxPriorityIntermediate.setValue(Tuple.of(query, data.getHypermaxPriorityList(requireNonNull(superRareHypermaxPriorityTypeLiveData.getValue()), Hypermax.UnitType.SUPER_RARE))));
        superRareHypermaxPriorityIntermediate.addSource(superRareHypermaxPriorityTypeLiveData, type -> superRareHypermaxPriorityIntermediate.setValue(Tuple.of(requireNonNull(superRareHypermaxPriorityQueryLiveData.getValue()), data.getHypermaxPriorityList(type, Hypermax.UnitType.SUPER_RARE))));
        superRareHypermaxPriority = Transformations.map(superRareHypermaxPriorityIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var specialHypermaxPriorityQueryLiveData = savedStateHandle.getLiveData(SPECIAL_HP_QUERY_KEY, "");
        final var specialHypermaxPriorityTypeLiveData = savedStateHandle.getLiveData(SPECIAL_HP_PRIORITY_KEY, Hypermax.Priority.MAX);
        specialHypermaxPriorityIntermediate = new MediatorLiveData<>();
        specialHypermaxPriorityIntermediate.addSource(specialHypermaxPriorityQueryLiveData, query -> specialHypermaxPriorityIntermediate.setValue(Tuple.of(query, data.getHypermaxPriorityList(requireNonNull(specialHypermaxPriorityTypeLiveData.getValue()), Hypermax.UnitType.SPECIAL))));
        specialHypermaxPriorityIntermediate.addSource(specialHypermaxPriorityTypeLiveData, type -> specialHypermaxPriorityIntermediate.setValue(Tuple.of(requireNonNull(specialHypermaxPriorityQueryLiveData.getValue()), data.getHypermaxPriorityList(type, Hypermax.UnitType.SPECIAL))));
        specialHypermaxPriority = Transformations.map(specialHypermaxPriorityIntermediate, pair -> getSearchResults(pair._2, pair._1));
    }

    private final SavedStateHandle savedStateHandle;
    private final UnitData data;
    private final UnitExplanationSupplier explanationSupplier;
    private final FavoritesDataRepository repository;
    private final FutureContainer container = new ViewModelFutureContainer(this);
    private final MediatorLiveData<Tuple2<String, ImmutableList<Unit>>> rareHypermaxPriorityIntermediate;
    private final MediatorLiveData<Tuple2<String, ImmutableList<Unit>>> superRareHypermaxPriorityIntermediate;
    private final MediatorLiveData<Tuple2<String, ImmutableList<Unit>>> specialHypermaxPriorityIntermediate;
    private final LiveData<ImmutableList<Unit>> rareHypermaxPriority;
    private final LiveData<ImmutableList<Unit>> superRareHypermaxPriority;
    private final LiveData<ImmutableList<Unit>> specialHypermaxPriority;

    public void setHypermaxPriorityQuery(Hypermax.UnitType type, String query) {
        final var key = switch (type) {
            case SPECIAL -> SPECIAL_HP_QUERY_KEY;
            case RARE -> RARE_HP_QUERY_KEY;
            case SUPER_RARE -> SUPER_RARE_HP_QUERY_KEY;
        };
        savedStateHandle.set(key, query);
    }

    public void setHypermaxPriorityType(Hypermax.UnitType type, Hypermax.Priority priority) {
        final var key = switch (type) {
            case SPECIAL -> SPECIAL_HP_PRIORITY_KEY;
            case RARE -> RARE_HP_PRIORITY_KEY;
            case SUPER_RARE -> SUPER_RARE_HP_PRIORITY_KEY;
        };
        savedStateHandle.set(key, priority);
    }

    public LiveData<ImmutableList<Unit>> getHypermaxPriorityLiveData(Hypermax.UnitType type) {
        return switch (type) {
            case SPECIAL -> specialHypermaxPriority;
            case RARE -> rareHypermaxPriority;
            case SUPER_RARE -> superRareHypermaxPriority;
        };
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, String query) {
        return getSearchResults(originalList, Collections.emptySet(), query);
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, Set<UnitBaseData.Type> types, String query) {
        final var filters = new HashSet<Predicate<Unit>>();
        FilterUnitByName.safeAddTo(filters, query, explanationSupplier);
        FilterUnitByType.safeAddTo(filters, types);
        return data.filterUnits(originalList, filters);
    }

    public ListenableFuture<@Nullable FavoriteItem> findFavoriteByUnitId(int id) {
        return container.addAndReturn(repository.findFavoriteByUnitId(id));
    }

    public ListenableFuture<List<FavoriteReason>> getReasonsForFavoriteSnapshot(int id) {
        return container.addAndReturn(repository.getReasonsForFavoriteSnapshot(id));
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> addFavorite(FavoriteItem item, Iterable<FavoriteReason> reasons) {
        return FluentFuture.from(repository.addFavorites(List.of(item)))
                .transformAsync(ignored -> repository.addFavoriteReasons(reasons), MoreExecutors.directExecutor());
    }

    public static final ViewModelInitializer<HypermaxPriorityTabViewModel> INITIALIZER = new ViewModelInitializer<>(HypermaxPriorityTabViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new HypermaxPriorityTabViewModel(
                SavedStateHandleSupport.createSavedStateHandle(creationExtras),
                myApp.getUnitData(),
                myApp.getUnitExplanationData(),
                myApp.getFavoritesDataRepository()
        );
    });

    public static HypermaxPriorityTabViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(HypermaxPriorityTabViewModel.class);
    }

}
