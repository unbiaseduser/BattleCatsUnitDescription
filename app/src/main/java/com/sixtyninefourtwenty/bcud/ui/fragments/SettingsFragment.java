package com.sixtyninefourtwenty.bcud.ui.fragments;

import static java.util.Objects.requireNonNull;

import android.os.Bundle;

import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;

import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.custompreferences.PreferenceFragmentCompatAccommodateCustomDialogPreferences;
import com.sixtyninefourtwenty.custompreferences.PreferenceHelper;
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferenceUI;

import org.checkerframework.checker.nullness.qual.Nullable;

public final class SettingsFragment extends PreferenceFragmentCompatAccommodateCustomDialogPreferences {

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String rootKey) {
        PreferenceHelper.installConfigurationChangePatch(this);
        setPreferencesFromResource(R.xml.preferences, rootKey);
        setupAppearanceNavigationPref();

        final var datasetPref = requireNonNull(this.<Preference>findPreference("dataset"), formatErrorMsg("dataset preference"));
        datasetPref.setOnPreferenceClickListener(preference -> {
            NavHostFragment.findNavController(this).navigate(SettingsFragmentDirections.actionNavSettingsToNavDatasetSettings());
            return true;
        });
    }

    private void setupAppearanceNavigationPref() {
        final PreferenceCategory appearanceCategory = requireNonNull(findPreference("navigation_appearance_category"), formatErrorMsg("navigation appearance category"));
        ThemingPreferenceUI.addThemingPreferencesWithDefaultSettings(
                appearanceCategory,
                requireActivity(),
                MyApplication.get(requireContext()).getPrefs()
        );
    }

    private String formatErrorMsg(String arg) {
        return String.format("Wrong key passed for %s", arg);
    }

}
