package com.sixtyninefourtwenty.bcud.ui.fragments.helppinsdetails.talentpriority;

import static java.util.Objects.requireNonNull;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByName;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByType;
import com.sixtyninefourtwenty.bcud.repository.UnitData;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.Talent;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.Tuple3;

@NonNullTypesByDefault
public final class TalentPriorityTabViewModel extends ViewModel {

    private static final String NON_UBER_TP_QUERY_KEY = "non_uber_tp_query";
    private static final String NON_UBER_TP_PRIORITY_KEY = "non_uber_tp_priority";
    private static final String UBER_TP_QUERY_KEY = "uber_tp_query";
    private static final String UBER_TP_PRIORITY_KEY = "uber_tp_priority";

    public TalentPriorityTabViewModel(SavedStateHandle savedStateHandle, UnitData data, UnitExplanationSupplier explanationSupplier) {
        this.savedStateHandle = savedStateHandle;
        this.data = data;
        this.explanationSupplier = explanationSupplier;
        final var nonUberTalentPriorityQueryLiveData = savedStateHandle.getLiveData(NON_UBER_TP_QUERY_KEY, "");
        final var nonUberTalentPriorityLiveData = savedStateHandle.getLiveData(NON_UBER_TP_PRIORITY_KEY, Talent.Priority.TOP);
        nonUberTalentPriorityIntermediate = new MediatorLiveData<>();
        nonUberTalentPriorityIntermediate.addSource(nonUberTalentPriorityQueryLiveData, query -> {
            final var talentPriority = requireNonNull(nonUberTalentPriorityLiveData.getValue());
            nonUberTalentPriorityIntermediate.setValue(Tuple.of(query, data.getTalentPriorityList(talentPriority, Talent.UnitType.NON_UBER), talentPriority));
        });
        nonUberTalentPriorityIntermediate.addSource(nonUberTalentPriorityLiveData, priority -> nonUberTalentPriorityIntermediate.setValue(Tuple.of(requireNonNull(nonUberTalentPriorityQueryLiveData.getValue()), data.getTalentPriorityList(priority, Talent.UnitType.NON_UBER), priority)));
        nonUberTalentPriority = Transformations.map(nonUberTalentPriorityIntermediate, triple -> Tuple.of(getSearchResults(triple._2, triple._1), triple._3));
        final var uberTalentPriorityQueryLiveData = savedStateHandle.getLiveData(UBER_TP_QUERY_KEY, "");
        final var uberTalentPriorityLiveData = savedStateHandle.getLiveData(UBER_TP_PRIORITY_KEY, Talent.Priority.TOP);
        uberTalentPriorityIntermediate = new MediatorLiveData<>();
        uberTalentPriorityIntermediate.addSource(uberTalentPriorityQueryLiveData, query -> {
            final var talentPriority = requireNonNull(uberTalentPriorityLiveData.getValue());
            uberTalentPriorityIntermediate.setValue(Tuple.of(query, data.getTalentPriorityList(talentPriority, Talent.UnitType.UBER), talentPriority));
        });
        uberTalentPriorityIntermediate.addSource(uberTalentPriorityLiveData, priority -> uberTalentPriorityIntermediate.setValue(Tuple.of(requireNonNull(uberTalentPriorityQueryLiveData.getValue()), data.getTalentPriorityList(priority, Talent.UnitType.UBER), priority)));
        uberTalentPriority = Transformations.map(uberTalentPriorityIntermediate, triple -> Tuple.of(getSearchResults(triple._2, triple._1), triple._3));
    }

    private final SavedStateHandle savedStateHandle;
    private final UnitData data;
    private final UnitExplanationSupplier explanationSupplier;
    private final MediatorLiveData<Tuple3<String, ImmutableList<Unit>, Talent.Priority>> nonUberTalentPriorityIntermediate;
    private final MediatorLiveData<Tuple3<String, ImmutableList<Unit>, Talent.Priority>> uberTalentPriorityIntermediate;
    private final LiveData<Tuple2<ImmutableList<Unit>, Talent.Priority>> nonUberTalentPriority;
    private final LiveData<Tuple2<ImmutableList<Unit>, Talent.Priority>> uberTalentPriority;

    public void setTalentPriorityQuery(Talent.UnitType type, String query) {
        final var key = switch (type) {
            case NON_UBER -> NON_UBER_TP_QUERY_KEY;
            case UBER -> UBER_TP_QUERY_KEY;
        };
        savedStateHandle.set(key, query);
    }

    public void setTalentPriorityType(Talent.UnitType type, Talent.Priority priority) {
        final var key = switch (type) {
            case NON_UBER -> NON_UBER_TP_PRIORITY_KEY;
            case UBER -> UBER_TP_PRIORITY_KEY;
        };
        savedStateHandle.set(key, priority);
    }

    public LiveData<Tuple2<ImmutableList<Unit>, Talent.Priority>> getTalentPriorityLiveData(Talent.UnitType type) {
        return switch (type) {
            case NON_UBER -> nonUberTalentPriority;
            case UBER -> uberTalentPriority;
        };
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, String query) {
        return getSearchResults(originalList, Collections.emptySet(), query);
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, Set<UnitBaseData.Type> types, String query) {
        final var filters = new HashSet<Predicate<Unit>>();
        FilterUnitByName.safeAddTo(filters, query, explanationSupplier);
        FilterUnitByType.safeAddTo(filters, types);
        return data.filterUnits(originalList, filters);
    }

    public static final ViewModelInitializer<TalentPriorityTabViewModel> INITIALIZER = new ViewModelInitializer<>(TalentPriorityTabViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new TalentPriorityTabViewModel(SavedStateHandleSupport.createSavedStateHandle(creationExtras), myApp.getUnitData(), myApp.getUnitExplanationData());
    });

    public static TalentPriorityTabViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(TalentPriorityTabViewModel.class);
    }

}
