package com.sixtyninefourtwenty.bcud.ui.activities;

import static java.util.Objects.requireNonNull;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.NotificationChannelCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.WindowInsetsControllerCompat;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.databinding.ActivityMainBinding;
import com.sixtyninefourtwenty.bcud.utils.AppPreferences;
import com.sixtyninefourtwenty.bcud.utils.Constants;
import com.sixtyninefourtwenty.theming.ActivityTheming;
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferencesSuppliers;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class MainActivity extends AppCompatActivity {

    private AppPreferences prefs;
    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    @NonNull
    public CoordinatorLayout getRootView() {
        return binding.getRoot();
    }

    @NonNull
    public Toolbar getToolbar() {
        return binding.appBarMain.toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        createNotificationChannels();
        prefs = AppPreferences.get(this);
        setFirstInstallSettings();
        copyThemingPreferences();
        ActivityTheming.applyTheming(this,
                R.style.Theme_BattleCatsUnitDescription,
                R.style.Theme_BattleCatsUnitDescription_Material3_Android11,
                R.style.Theme_BattleCatsUnitDescription_Material3,
                prefs
        );
        super.onCreate(savedInstanceState);

        final var w = requireNonNull(getWindow());
        w.setStatusBarColor(Color.TRANSPARENT);
        final var isDarkMode = (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
        final var wic = new WindowInsetsControllerCompat(w, w.getDecorView());
        wic.setAppearanceLightStatusBars(!isDarkMode);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarMain.toolbar);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_udp, R.id.nav_help_pins, R.id.nav_guides, R.id.nav_settings, R.id.nav_about, R.id.nav_misc)
                .setOpenableLayout(binding.drawerLayout)
                .build();
        final var navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
        loadData();
        getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (binding.drawerLayout.isOpen()) {
                    binding.drawerLayout.close();
                } else {
                    remove();
                    getOnBackPressedDispatcher().onBackPressed();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        final var navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void createNotificationChannels() {
        NotificationManagerCompat.from(this)
                .createNotificationChannel(new NotificationChannelCompat.Builder(Constants.FAVORITES_BACKUP_NOTIFICATION_CHANNEL_ID, NotificationManagerCompat.IMPORTANCE_LOW)
                        .setName(getString(R.string.favorites_backup))
                        .setDescription(getString(R.string.favorites_notification_channel_desc))
                        .build());
    }

    private void loadData() {
        MyApplication.get(this).eagerInit();
    }

    private void setFirstInstallSettings() {
        AppPreferences.initializePreferences(this);
        if (prefs.getFirstLaunch()) {
            prefs.setFirstLaunch(false);
        }
    }

    private void copyThemingPreferences() {
        if (!prefs.isThemingPreferencesMigrated()) {
            ThemingPreferencesSuppliers.copyFromDefaultThemingPreferences(
                    this,
                    prefs::setMd3,
                    prefs::setThemeColor,
                    prefs::setLightDarkMode,
                    prefs::setUseM3CustomColorThemeOnAndroid12
            );
            prefs.setThemingPreferencesMigrated(true);
        }
    }

}
