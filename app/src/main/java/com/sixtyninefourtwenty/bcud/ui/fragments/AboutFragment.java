package com.sixtyninefourtwenty.bcud.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.navigation.fragment.NavHostFragment;

import com.danielstone.materialaboutlibrary.ConvenienceBuilder;
import com.danielstone.materialaboutlibrary.MaterialAboutFragment;
import com.danielstone.materialaboutlibrary.items.MaterialAboutActionItem;
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrary.model.MaterialAboutList;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.databinding.DialogAboutDevBinding;
import com.sixtyninefourtwenty.bcud.enums.License;
import com.sixtyninefourtwenty.bcud.utils.AssetImageLoading;
import com.sixtyninefourtwenty.bcud.utils.Utils;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingDialogFragment;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public final class AboutFragment extends MaterialAboutFragment {

    @Override
    protected boolean shouldAnimate() {
        return false;
    }

    @Override
    protected MaterialAboutList getMaterialAboutList(Context activityContext) {
        final var nav = NavHostFragment.findNavController(this);
        return new MaterialAboutList.Builder()
                .addCard(new MaterialAboutCard.Builder()
                        .title(R.string.app_info)
                        .addItem(ConvenienceBuilder.createAppTitleItem(activityContext))
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.gavel)
                                .text(R.string.app_license)
                                .subText(R.string.gpl_3)
                                .setOnClickAction(() -> Utils.handleOpenWebsite(this, License.GPL3.getLicenseUrl()))
                                .build())
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.description)
                                .text(R.string.third_party_licenses)
                                .subText(R.string.third_party_licenses_subtext)
                                .setOnClickAction(() -> nav.navigate(AboutFragmentDirections.actionNavAboutToNavThirdPartyLicenses()))
                                .build())
                        .build())
                .addCard(new MaterialAboutCard.Builder()
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.person)
                                .text(getString(R.string.about_dev))
                                .setOnClickAction(() -> new AboutDevDialog().show(getChildFragmentManager(), null))
                                .build())
                        .build())
                .addCard(new MaterialAboutCard.Builder()
                        .title(R.string.udp)
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.gavel)
                                .text(R.string.license)
                                .subText(R.string.cc_by_nc_sa_4)
                                .setOnClickAction(() -> Utils.handleOpenWebsite(this, "https://creativecommons.org/licenses/by-nc-sa/4.0/"))
                                .build())
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.description)
                                .text(R.string.udp_credits)
                                .setOnClickAction(() -> Utils.handleOpenWebsite(this, "https://thanksfeanor.pythonanywhere.com/UDP/credits"))
                                .build())
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.chat)
                                .text(R.string.udp_feedback)
                                .setOnClickAction(() -> Utils.handleOpenWebsite(this, "https://thanksfeanor.pythonanywhere.com/Feedback"))
                                .build())
                        .addItem(new MaterialAboutActionItem.Builder()
                                .icon(R.drawable.attach_money)
                                .text(R.string.udp_donations)
                                .setOnClickAction(() -> Utils.handleOpenWebsite(this, "https://thanksfeanor.pythonanywhere.com/Donations"))
                                .build())
                        .build())
                .build();
    }

    public static final class AboutDevDialog extends BaseViewBindingDialogFragment<DialogAboutDevBinding> {

        @Override
        protected @NonNull DialogAboutDevBinding initBinding(@NonNull LayoutInflater inflater) {
            return DialogAboutDevBinding.inflate(inflater);
        }

        @Override
        protected @NonNull Dialog initDialog(@NonNull DialogAboutDevBinding binding, @Nullable Bundle savedInstanceState) {
            AssetImageLoading.loadAssetImage(binding.icon, "img/lilmohawkcat.png");
            binding.gitlab.setOnClickListener(v -> openWebsite("https://gitlab.com/unbiaseduser"));
            return new MaterialAlertDialogBuilder(requireContext())
                    .setView(binding.getRoot())
                    .setPositiveButton(R.string.got_it, null)
                    .create();
        }

    }

}
