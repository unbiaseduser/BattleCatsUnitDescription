package com.sixtyninefourtwenty.bcud.ui.fragments.unitdesc;

import static com.sixtyninefourtwenty.common.interfaces.MyMenuItemClickListener.voidReturning;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.kennyc.view.MultiStateView;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.adapters.UnitListAdapterGrid;
import com.sixtyninefourtwenty.bcud.adapters.UnitListAdapterList;
import com.sixtyninefourtwenty.bcud.databinding.GenericSearchableListBinding;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite.AddFavoriteDialog;
import com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite.EditFavoriteDialog;
import com.sixtyninefourtwenty.bcud.ui.fragments.unitdesc.root.UnitDescFragment;
import com.sixtyninefourtwenty.bcud.ui.fragments.unitdesc.root.UnitDescFragmentDirections;
import com.sixtyninefourtwenty.bcud.utils.MultiFragmentViewModelStoreOwner;
import com.sixtyninefourtwenty.bcud.utils.RemovableListenerListenableFutureWrapper;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingFragment;
import com.sixtyninefourtwenty.bcud.viewmodels.AppSettingsViewModel;
import com.sixtyninefourtwenty.bcud.viewmodels.SearchViewModel;
import com.sixtyninefourtwenty.common.concurrent.FutureContainer;
import com.sixtyninefourtwenty.common.concurrent.LifecycleAwareFutureContainer;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;
import com.sixtyninefourtwenty.common.utils.MoreFutures;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.function.BiConsumer;

import me.zhanghai.android.fastscroll.FastScrollerBuilder;

public abstract class UnitDescTabFragment extends BaseViewBindingFragment<@NonNull GenericSearchableListBinding> {
    protected abstract UnitBaseData.Type getUnitType();

    private FutureContainer futureContainer;
    private RemovableListenerListenableFutureWrapper.Container futureWrapperContainer;

    @Override
    protected @NonNull GenericSearchableListBinding initBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return GenericSearchableListBinding.inflate(inflater, container, false);
    }

    @Override
    protected void setup(@NonNull GenericSearchableListBinding binding, @Nullable Bundle savedInstanceState) {
        futureWrapperContainer = new RemovableListenerListenableFutureWrapper.Container(getViewLifecycleOwner());
        futureContainer = new LifecycleAwareFutureContainer(getViewLifecycleOwner());
        final var searchModel = SearchViewModel.get(new MultiFragmentViewModelStoreOwner(UnitDescFragment.VIEWMODEL_TAG, this));
        final var settingsModel = AppSettingsViewModel.get(requireActivity());
        final var viewModel = UnitDescTabViewModel.get(this);
        final var rcv = binding.list;
        rcv.setHasFixedSize(true);
        new FastScrollerBuilder(rcv).build();
        final var decoration = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);

        final BiConsumer<View, Unit> onItemLongClick = (v, unit) -> {
            final var popupMenu = new PopupMenu(requireContext(), v);
            final var menu = popupMenu.getMenu();
            menu.add(R.string.add_to_favorites)
                    .setIcon(R.drawable.star)
                    .setOnMenuItemClickListener(voidReturning(item -> {
                        MoreFutures.addIgnoreExceptionsCallback(
                                futureContainer.addAndReturn(viewModel.findFavoriteByUnitId(unit.getId())),
                                existingItem -> {
                                    if (existingItem != null) {
                                        MoreFutures.addIgnoreExceptionsCallback(
                                                futureContainer.addAndReturn(viewModel.getReasonsForFavoriteSnapshot(unit.getId())),
                                                reasons -> navigate(UnitDescFragmentDirections.showEditFavoriteDialog(unit, reasons.toArray(new FavoriteReason[0]))),
                                                ContextCompat.getMainExecutor(requireContext())
                                        );
                                    } else {
                                        navigate(UnitDescFragmentDirections.showAddFavoriteDialog(unit));
                                    }
                                },
                                ContextCompat.getMainExecutor(requireContext())
                        );
                    }));
            popupMenu.setForceShowIcon(true);
            popupMenu.show();
        };

        final var adapterGridView = new UnitListAdapterGrid(unit -> navigate(UnitDescFragmentDirections.goToUnitInfo(unit)),
                onItemLongClick);
        final var adapterListView = new UnitListAdapterList(unit -> navigate(UnitDescFragmentDirections.goToUnitInfo(unit)),
                onItemLongClick);
        settingsModel.getListViewMode().observe(getViewLifecycleOwner(), mode -> {
            switch (mode) {
                case LIST -> {
                    rcv.setLayoutManager(new LinearLayoutManager(requireContext()));
                    rcv.setAdapter(adapterListView);
                    rcv.addItemDecoration(decoration);
                }
                case GRID -> {
                    rcv.setLayoutManager(new GridLayoutManager(requireContext(), UnitListAdapterGrid.getGridSpans(requireContext())));
                    rcv.setAdapter(adapterGridView);
                    rcv.removeItemDecoration(decoration);
                }
            }
        });
        searchModel.getQuery().observe(getViewLifecycleOwner(), query -> viewModel.setUnitDescQuery(getUnitType(), query));
        viewModel.getUnitDescLiveData(getUnitType()).observe(getViewLifecycleOwner(), list -> {
            adapterListView.submitList(list);
            adapterGridView.submitList(list);
            binding.getRoot().setViewState(list.isEmpty() ? MultiStateView.ViewState.EMPTY : MultiStateView.ViewState.CONTENT);
        });

        AddFavoriteDialog.registerCallback(
                requireActivity(),
                getViewLifecycleOwner(),
                (favoriteItem, favoriteReasons) ->
                        futureWrapperContainer.add(viewModel.addFavorite(favoriteItem, favoriteReasons))
                                .addListener(ignored -> showToast(R.string.add_favorite_success),
                                        ContextCompat.getMainExecutor(requireContext()))
        );

        EditFavoriteDialog.registerCallback(
                requireActivity(),
                getViewLifecycleOwner(),
                (favoriteItem, favoriteReasons) ->
                        futureWrapperContainer.add(viewModel.addFavorite(favoriteItem, favoriteReasons))
                                .addListener(ignored -> showToast(R.string.update_favorite_success),
                                        ContextCompat.getMainExecutor(requireContext()))
        );
    }

}
