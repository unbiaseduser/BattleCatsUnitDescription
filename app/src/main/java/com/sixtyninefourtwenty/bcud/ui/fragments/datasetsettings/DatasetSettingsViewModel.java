package com.sixtyninefourtwenty.bcud.ui.fragments.datasetsettings;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.utils.DatasetPreferences;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;

@NonNullTypesByDefault
public final class DatasetSettingsViewModel extends ViewModel {

    private final DatasetPreferences preferences;
    private final SavedStateHandle savedStateHandle;
    private final MediatorLiveData<Boolean> isStateChanged = new MediatorLiveData<>(false);

    public DatasetSettingsViewModel(DatasetPreferences preferences, SavedStateHandle savedStateHandle) {
        this.preferences = preferences;
        this.savedStateHandle = savedStateHandle;
        if (!savedStateHandle.contains("unit_tf_material_data")) {
            setUnitTFMaterialData(preferences.getUnitTFMaterialData());
        }
        if (!savedStateHandle.contains("unit_talent_data")) {
            setUnitTalentData(preferences.getUnitTalentData());
        }
        if (!savedStateHandle.contains("unit_hp_data")) {
            setUnitHPData(preferences.getUnitHPData());
        }
        if (!savedStateHandle.contains("unit_eep_data")) {
            setUnitEEPData(preferences.getUnitEEPData());
        }
        isStateChanged.addSource(savedStateHandle.getLiveData("unit_tf_material_data"), value -> isStateChanged.setValue(isStateChanged()));
        isStateChanged.addSource(savedStateHandle.getLiveData("unit_talent_data"), value -> isStateChanged.setValue(isStateChanged()));
        isStateChanged.addSource(savedStateHandle.getLiveData("unit_hp_data"), value -> isStateChanged.setValue(isStateChanged()));
        isStateChanged.addSource(savedStateHandle.getLiveData("unit_eep_data"), value -> isStateChanged.setValue(isStateChanged()));
    }

    public void persistData(ContentResolver contentResolver) {
        final var unitTFMaterialData = getUnitTFMaterialData();
        if (unitTFMaterialData != null) {
            contentResolver.takePersistableUriPermission(unitTFMaterialData, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        preferences.setUnitTFMaterialData(unitTFMaterialData);
        final var unitTalentData = getUnitTalentData();
        if (unitTalentData != null) {
            contentResolver.takePersistableUriPermission(unitTalentData, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        preferences.setUnitTalentData(unitTalentData);
        final var unitHPData = getUnitHPData();
        if (unitHPData != null) {
            contentResolver.takePersistableUriPermission(unitHPData, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        preferences.setUnitHPData(unitHPData);
        final var unitEEPData = getUnitEEPData();
        if (unitEEPData != null) {
            contentResolver.takePersistableUriPermission(unitEEPData, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        preferences.setUnitEEPData(unitEEPData);
    }

    public LiveData<Boolean> getStateChanged() {
        return isStateChanged;
    }

    private boolean isStateChanged() {
        if (!Objects.equals(getUnitTFMaterialData(), preferences.getUnitTFMaterialData())) {
            return true;
        }
        if (!Objects.equals(getUnitTalentData(), preferences.getUnitTalentData())) {
            return true;
        }
        if (!Objects.equals(getUnitHPData(), preferences.getUnitHPData())) {
            return true;
        }
        return !Objects.equals(getUnitEEPData(), preferences.getUnitEEPData());
    }

    public LiveData<@Nullable Uri> getUnitTFMaterialDataLiveData() {
        return savedStateHandle.getLiveData("unit_tf_material_data");
    }

    @Nullable
    public Uri getUnitTFMaterialData() {
        return savedStateHandle.get("unit_tf_material_data");
    }

    public void setUnitTFMaterialData(@Nullable Uri uri) {
        savedStateHandle.set("unit_tf_material_data", uri);
    }

    public LiveData<@Nullable Uri> getUnitTalentDataLiveData() {
        return savedStateHandle.getLiveData("unit_talent_data");
    }

    @Nullable
    public Uri getUnitTalentData() {
        return savedStateHandle.get("unit_talent_data");
    }

    public void setUnitTalentData(@Nullable Uri uri) {
        savedStateHandle.set("unit_talent_data", uri);
    }

    public LiveData<@Nullable Uri> getUnitHPDataLiveData() {
        return savedStateHandle.getLiveData("unit_hp_data");
    }

    @Nullable
    public Uri getUnitHPData() {
        return savedStateHandle.get("unit_hp_data");
    }

    public void setUnitHPData(@Nullable Uri uri) {
        savedStateHandle.set("unit_hp_data", uri);
    }

    public LiveData<@Nullable Uri> getUnitEEPDataLiveData() {
        return savedStateHandle.getLiveData("unit_eep_data");
    }

    @Nullable
    public Uri getUnitEEPData() {
        return savedStateHandle.get("unit_eep_data");
    }

    public void setUnitEEPData(@Nullable Uri uri) {
        savedStateHandle.set("unit_eep_data", uri);
    }

    private static final ViewModelInitializer<DatasetSettingsViewModel> INITIALIZER = new ViewModelInitializer<>(DatasetSettingsViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new DatasetSettingsViewModel(myApp.getDataSetPreferences(), SavedStateHandleSupport.createSavedStateHandle(creationExtras));
    });

    public static DatasetSettingsViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(DatasetSettingsViewModel.class);
    }

}
