package com.sixtyninefourtwenty.bcud.ui.dialogs.addeditfavorite;

import static com.sixtyninefourtwenty.common.interfaces.MyMenuItemClickListener.voidReturning;
import static java.util.Objects.requireNonNull;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.StringRes;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.os.BundleCompat;
import androidx.navigation.NavDirections;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.adapters.FavoriteReasonAdapter;
import com.sixtyninefourtwenty.bcud.databinding.DialogAddEditFavoriteBinding;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.ui.dialogs.AddReasonDialog;
import com.sixtyninefourtwenty.bcud.ui.dialogs.EditReasonDialog;
import com.sixtyninefourtwenty.bcud.utils.AssetImageLoading;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingBottomSheetAlertDialogFragment;
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder;
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class AbstractAddEditFavoriteDialog extends BaseViewBindingBottomSheetAlertDialogFragment<@NonNull DialogAddEditFavoriteBinding> {

    @StringRes
    protected abstract int getTitle();

    @NonNull
    protected abstract Unit getUnit();

    @Nullable
    protected abstract List<@NonNull FavoriteReason> getExistingReasons();

    @NonNull
    protected abstract NavDirections getAddReasonDialogDirections(int unitId);

    @NonNull
    protected abstract NavDirections getEditReasonDialogDirections(FavoriteReason reason);

    protected abstract void onFavoriteCreated(@NonNull FavoriteItem favoriteItem, @NonNull List<@NonNull FavoriteReason> favoriteReasons);

    private final FavoriteReasonAdapter adapter = new FavoriteReasonAdapter((adapter, v, reason) -> {
        final var popupMenu = new PopupMenu(requireContext(), v);
        final var menu = popupMenu.getMenu();
        menu.add(R.string.edit)
                .setIcon(R.drawable.edit)
                .setOnMenuItemClickListener(voidReturning(item -> navigate(getEditReasonDialogDirections(reason))));
        menu.add(R.string.delete)
                .setIcon(R.drawable.delete)
                .setOnMenuItemClickListener(voidReturning(item -> new MaterialAlertDialogBuilder(requireContext())
                        .setTitle(R.string.delete)
                        .setMessage(R.string.confirm_delete)
                        .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                            updateList(list -> list.remove(reason));
                            showToast(R.string.delete_success);
                        })
                        .show()));
        popupMenu.setForceShowIcon(true);
        popupMenu.show();
    });

    @Override
    protected @NonNull DialogAddEditFavoriteBinding initBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return DialogAddEditFavoriteBinding.inflate(inflater, container, false);
    }

    @Override
    @NonNull
    protected View initDialogView(@NonNull DialogAddEditFavoriteBinding binding) {
        return new BottomSheetAlertDialogFragmentViewBuilder(binding.getRoot(), this)
                .setTitle(getString(getTitle()))
                .setPositiveButton(new DialogButtonProperties.Builder(getString(android.R.string.ok))
                        .setOnClickListener(() -> onFavoriteCreated(new FavoriteItem(getUnit().getId()), adapter.getCurrentList()))
                        .build())
                .setNegativeButton(new DialogButtonProperties.Builder(getString(android.R.string.cancel))
                        .build())
                .getRootView();
    }

    @Override
    protected void setup(@NonNull DialogAddEditFavoriteBinding binding, @Nullable Bundle savedInstanceState) {
        final var unit = getUnit();
        AssetImageLoading.loadAssetImage(binding.unitIconAlt, unit.getLatestFormIconPath(MyApplication.get(requireContext()).getUnitExplanationData()));
        binding.unitNameAlt.setText(unit.getExplanation(MyApplication.get(requireContext()).getUnitExplanationData()).getFirstFormName());
        binding.reasonsList.setAdapter(adapter);
        binding.addReason.setOnClickListener(v -> navigate(getAddReasonDialogDirections(unit.getId())));
        final var existingReasons = getExistingReasons();
        if (existingReasons != null) {
            adapter.submitList(existingReasons);
        }
        requireActivity().getSupportFragmentManager().setFragmentResultListener(AddReasonDialog.CALLBACK_KEY, getViewLifecycleOwner(), (requestKey, result) -> {
            final var reason = requireNonNull(BundleCompat.getParcelable(result, "reason", FavoriteReason.class));
            updateList(list -> list.add(reason));
        });
        requireActivity().getSupportFragmentManager().setFragmentResultListener(EditReasonDialog.CALLBACK_KEY, getViewLifecycleOwner(), (requestKey, result) -> {
            final var reason = requireNonNull(BundleCompat.getParcelable(result, "reason", FavoriteReason.class));
            updateList(list -> {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getUid() == reason.getUid()) {
                        list.set(i, reason);
                        break;
                    }
                }
            });
        });
    }

    private void updateList(Consumer<List<FavoriteReason>> mutations) {
        final var list = new ArrayList<>(adapter.getCurrentList());
        mutations.accept(list);
        adapter.submitList(list);
    }

}
