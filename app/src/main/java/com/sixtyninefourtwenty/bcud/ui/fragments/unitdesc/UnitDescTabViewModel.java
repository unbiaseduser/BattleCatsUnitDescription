package com.sixtyninefourtwenty.bcud.ui.fragments.unitdesc;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByName;
import com.sixtyninefourtwenty.bcud.objects.filters.unit.FilterUnitByType;
import com.sixtyninefourtwenty.bcud.repository.FavoritesDataRepository;
import com.sixtyninefourtwenty.bcud.repository.UnitData;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitExplanationSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.concurrent.FutureContainer;
import com.sixtyninefourtwenty.common.concurrent.ViewModelFutureContainer;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import io.vavr.Tuple;
import io.vavr.Tuple2;

@NonNullTypesByDefault
public final class UnitDescTabViewModel extends ViewModel {

    private static final String STORY_LEGEND_QUERY_KEY = "story_legend_query";
    private static final String CF_SPECIAL_QUERY_KEY = "cf_special_query";
    private static final String ADVENT_DROP_QUERY_KEY = "advent_drop_query";
    private static final String RARE_QUERY_KEY = "rare_query";
    private static final String SUPER_RARE_QUERY_KEY = "super_rare_query";
    private static final String UBER_QUERY_KEY = "uber_query";
    private static final String LEGEND_RARE_QUERY_KEY = "legend_rare_query";

    public UnitDescTabViewModel(
            SavedStateHandle savedStateHandle,
            UnitData data,
            UnitExplanationSupplier explanationSupplier,
            FavoritesDataRepository repository
    ) {
        this.savedStateHandle = savedStateHandle;
        this.data = data;
        this.explanationSupplier = explanationSupplier;
        this.repository = repository;
        final var storyLegendQueryLiveData = savedStateHandle.getLiveData(STORY_LEGEND_QUERY_KEY, "");
        storyLegendsIntermediate = Transformations.map(storyLegendQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.STORY_LEGEND)));
        storyLegends = Transformations.map(storyLegendsIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var cfSpecialsQueryLiveData = savedStateHandle.getLiveData(CF_SPECIAL_QUERY_KEY, "");
        cfSpecialsIntermediate = Transformations.map(cfSpecialsQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.CF_SPECIAL)));
        cfSpecials = Transformations.map(cfSpecialsIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var adventDropsQueryLiveData = savedStateHandle.getLiveData(ADVENT_DROP_QUERY_KEY, "");
        adventDropsIntermediate = Transformations.map(adventDropsQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.ADVENT_DROP)));
        adventDrops = Transformations.map(adventDropsIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var raresQueryLiveData = savedStateHandle.getLiveData(RARE_QUERY_KEY, "");
        raresIntermediate = Transformations.map(raresQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.RARE)));
        rares = Transformations.map(raresIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var superRaresQueryLiveData = savedStateHandle.getLiveData(SUPER_RARE_QUERY_KEY, "");
        superRaresIntermediate = Transformations.map(superRaresQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.SUPER_RARE)));
        superRares = Transformations.map(superRaresIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var ubersQueryLiveData = savedStateHandle.getLiveData(UBER_QUERY_KEY, "");
        ubersIntermediate = Transformations.map(ubersQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.UBER)));
        ubers = Transformations.map(ubersIntermediate, pair -> getSearchResults(pair._2, pair._1));
        final var legendRaresQueryLiveData = savedStateHandle.getLiveData(LEGEND_RARE_QUERY_KEY, "");
        legendRaresIntermediate = Transformations.map(legendRaresQueryLiveData, query -> Tuple.of(query, data.getMainList(UnitBaseData.Type.LEGEND_RARE)));
        legendRares = Transformations.map(legendRaresIntermediate, pair -> getSearchResults(pair._2, pair._1));
    }

    private final SavedStateHandle savedStateHandle;
    private final UnitData data;
    private final UnitExplanationSupplier explanationSupplier;
    private final FavoritesDataRepository repository;
    private final FutureContainer container = new ViewModelFutureContainer(this);
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> storyLegendsIntermediate;
    private final LiveData<ImmutableList<Unit>> storyLegends;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> cfSpecialsIntermediate;
    private final LiveData<ImmutableList<Unit>> cfSpecials;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> adventDropsIntermediate;
    private final LiveData<ImmutableList<Unit>> adventDrops;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> raresIntermediate;
    private final LiveData<ImmutableList<Unit>> rares;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> superRaresIntermediate;
    private final LiveData<ImmutableList<Unit>> superRares;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> ubersIntermediate;
    private final LiveData<ImmutableList<Unit>> ubers;
    private final LiveData<Tuple2<String, ImmutableList<Unit>>> legendRaresIntermediate;
    private final LiveData<ImmutableList<Unit>> legendRares;

    public void setUnitDescQuery(UnitBaseData.Type type, String query) {
        final var key = switch (type) {
            case STORY_LEGEND -> STORY_LEGEND_QUERY_KEY;
            case CF_SPECIAL -> CF_SPECIAL_QUERY_KEY;
            case ADVENT_DROP -> ADVENT_DROP_QUERY_KEY;
            case RARE -> RARE_QUERY_KEY;
            case SUPER_RARE -> SUPER_RARE_QUERY_KEY;
            case UBER -> UBER_QUERY_KEY;
            case LEGEND_RARE -> LEGEND_RARE_QUERY_KEY;
        };
        savedStateHandle.set(key, query);
    }

    public LiveData<ImmutableList<Unit>> getUnitDescLiveData(UnitBaseData.Type type) {
        return switch (type) {
            case STORY_LEGEND -> storyLegends;
            case CF_SPECIAL -> cfSpecials;
            case ADVENT_DROP -> adventDrops;
            case RARE -> rares;
            case SUPER_RARE -> superRares;
            case UBER -> ubers;
            case LEGEND_RARE -> legendRares;
        };
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, String query) {
        return getSearchResults(originalList, Collections.emptySet(), query);
    }

    private ImmutableList<Unit> getSearchResults(List<Unit> originalList, Set<UnitBaseData.Type> types, String query) {
        final var filters = new HashSet<Predicate<Unit>>();
        FilterUnitByName.safeAddTo(filters, query, explanationSupplier);
        FilterUnitByType.safeAddTo(filters, types);
        return data.filterUnits(originalList, filters);
    }

    public ListenableFuture<@Nullable FavoriteItem> findFavoriteByUnitId(int id) {
        return container.addAndReturn(repository.findFavoriteByUnitId(id));
    }

    public ListenableFuture<List<FavoriteReason>> getReasonsForFavoriteSnapshot(int id) {
        return container.addAndReturn(repository.getReasonsForFavoriteSnapshot(id));
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> addFavorite(FavoriteItem item, Iterable<FavoriteReason> reasons) {
        return FluentFuture.from(repository.addFavorites(List.of(item)))
                .transformAsync(ignored -> repository.addFavoriteReasons(reasons), MoreExecutors.directExecutor());
    }

    public static final ViewModelInitializer<UnitDescTabViewModel> INITIALIZER = new ViewModelInitializer<>(UnitDescTabViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new UnitDescTabViewModel(
                SavedStateHandleSupport.createSavedStateHandle(creationExtras),
                myApp.getUnitData(),
                myApp.getUnitExplanationData(),
                myApp.getFavoritesDataRepository()
        );
    });

    public static UnitDescTabViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(UnitDescTabViewModel.class);
    }

}
