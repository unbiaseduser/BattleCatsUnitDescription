package com.sixtyninefourtwenty.bcud.ui.fragments.guidedetails.other.combos;

import static com.sixtyninefourtwenty.common.interfaces.SimpleAdapterViewOnItemSelectedListener.simplify;
import static java.util.stream.Collectors.toCollection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.core.view.MenuProvider;

import com.google.android.material.chip.Chip;
import com.kennyc.view.MultiStateView;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.bcud.adapters.ComboListAdapter;
import com.sixtyninefourtwenty.bcud.databinding.DialogSearchFilterCombosBinding;
import com.sixtyninefourtwenty.bcud.databinding.FragmentCombosBinding;
import com.sixtyninefourtwenty.bcud.objects.Combo;
import com.sixtyninefourtwenty.bcud.objects.filters.ComboFilter;
import com.sixtyninefourtwenty.bcud.objects.filters.UnitFilter;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingBottomSheetAlertDialogFragment;
import com.sixtyninefourtwenty.bcud.utils.fragments.BaseViewBindingFragment;
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder;
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.EnumSet;
import java.util.List;

public final class CombosFragment extends BaseViewBindingFragment<@NonNull FragmentCombosBinding> {
    private final MenuProvider provider = new MenuProvider() {
        @Override
        public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
            menuInflater.inflate(R.menu.menu_combos, menu);
        }

        @Override
        public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
            int id = menuItem.getItemId();
            if (id == R.id.filter_combos) {
                navigate(CombosFragmentDirections.showSearchFilterCombosDialog());
                return true;
            }
            return false;
        }
    };

    @Override
    protected @NonNull FragmentCombosBinding initBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        return FragmentCombosBinding.inflate(inflater, container, false);
    }

    @Override
    protected void setup(@NonNull FragmentCombosBinding binding, @Nullable Bundle savedInstanceState) {
        final var model = CombosViewModel.get(requireActivity());
        final var adapter = new ComboListAdapter(unit -> navigate(CombosFragmentDirections.goToUnitInfoFromCombos(unit)));
        binding.list.setAdapter(adapter);
        model.getCombosLiveData().observe(getViewLifecycleOwner(), combos -> {
            binding.getRoot().setViewState(combos.isEmpty() ? MultiStateView.ViewState.EMPTY : MultiStateView.ViewState.CONTENT);
            adapter.submitList(combos);
        });
        requireActivity().addMenuProvider(provider, getViewLifecycleOwner());
    }

    public static final class SearchFilterCombosDialog extends BaseViewBindingBottomSheetAlertDialogFragment<@NonNull DialogSearchFilterCombosBinding> {

        private CombosViewModel model;

        private List<Chip> getUnitTypesChips(DialogSearchFilterCombosBinding binding) {
            return List.of(binding.storyLegends, binding.cfSpecials, binding.adventDrops, binding.rares,
                    binding.superRares, binding.ubers, binding.legendRares);
        }

        private List<Chip> getComboTypesChips(DialogSearchFilterCombosBinding binding) {
            return List.of(binding.atk, binding.def, binding.spd, binding.strong, binding.massive,
                    binding.resist, binding.kb, binding.slow, binding.freeze, binding.weaken,
                    binding.strengthen, binding.crit, binding.startCannon, binding.cannonAtk,
                    binding.recharge, binding.baseDef, binding.startMoney, binding.startLevel,
                    binding.wallet, binding.research, binding.accounting, binding.study);
        }

        @Override
        protected @NonNull DialogSearchFilterCombosBinding initBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
            return DialogSearchFilterCombosBinding.inflate(inflater, container, false);
        }

        @Override
        protected @NonNull View initDialogView(DialogSearchFilterCombosBinding binding) {
            return new BottomSheetAlertDialogFragmentViewBuilder(binding.getRoot(), this)
                    .setTitle(getString(R.string.search_filter_combos))
                    .setPositiveButton(new DialogButtonProperties.Builder(getString(R.string.search))
                            .setOnClickListener(() -> {
                                final var unitInputText = binding.unitInput.getText();
                                final String unitInput;
                                if (unitInputText == null || unitInputText.toString().isBlank()) {
                                    unitInput = null;
                                } else {
                                    unitInput = unitInputText.toString();
                                }
                                final var unitTypeChips = getUnitTypesChips(binding);
                                final var types = unitTypeChips.stream()
                                        .filter(Chip::isChecked)
                                        .map(chip -> (UnitBaseData.Type) chip.getTag())
                                        .collect(toCollection(() -> EnumSet.noneOf(UnitBaseData.Type.class)));
                                model.setUnitFilter(UnitFilter.of(unitInput, types));

                                final var nameInputText = binding.nameInput.getText();
                                final String nameInput;
                                if (nameInputText == null || nameInputText.toString().isBlank()) {
                                    nameInput = null;
                                } else {
                                    nameInput = nameInputText.toString();
                                }
                                final var comboTypeChips = getComboTypesChips(binding);
                                final var comboTypes = comboTypeChips.stream()
                                        .filter(Chip::isChecked)
                                        .map(chip -> (Combo.Type) chip.getTag())
                                        .collect(toCollection(() -> EnumSet.noneOf(Combo.Type.class)));
                                model.setComboFilter(ComboFilter.of(
                                        nameInput,
                                        comboTypes,
                                        new ComboFilter.UnitFilterArgs(
                                                null, /* units are handled by the viewmodel */
                                                switch (binding.filterUnitConditionPicker.getSelectedItemPosition()) {
                                                    case 0 -> ComboFilter.UnitFilterArgs.Type.ANY;
                                                    case 1 -> ComboFilter.UnitFilterArgs.Type.ALL;
                                                    default -> throw new AssertionError("Unrecognized position: " + binding.filterUnitConditionPicker.getSelectedItemPosition());
                                                }
                                        )

                                ));
                            })
                            .build())
                    .setNeutralButton(new DialogButtonProperties.Builder(getString(R.string.reset))
                            .setOnClickListener(() -> model.resetSearchFilterCombosState())
                            .build())
                    .setNegativeButton(DialogButtonProperties.ofOnlyText(getString(android.R.string.cancel)))
                    .getRootView();
        }

        @Override
        protected void setup(@NonNull DialogSearchFilterCombosBinding binding, @Nullable Bundle savedInstanceState) {
            model = CombosViewModel.get(requireActivity());
            setupViews(model, binding);
        }

        private void setupViews(@NonNull CombosViewModel model, @NonNull DialogSearchFilterCombosBinding binding) {
            binding.byNamesExpander.setOnClickListener(v -> binding.expandByNames.toggle());
            binding.byUnitsExpander.setOnClickListener(v -> binding.expandByUnits.toggle());
            binding.byTypesExpander.setOnClickListener(v -> binding.expandByTypes.toggle());
            binding.storyLegends.setTag(UnitBaseData.Type.STORY_LEGEND);
            binding.cfSpecials.setTag(UnitBaseData.Type.CF_SPECIAL);
            binding.adventDrops.setTag(UnitBaseData.Type.ADVENT_DROP);
            binding.rares.setTag(UnitBaseData.Type.RARE);
            binding.superRares.setTag(UnitBaseData.Type.SUPER_RARE);
            binding.ubers.setTag(UnitBaseData.Type.UBER);
            binding.legendRares.setTag(UnitBaseData.Type.LEGEND_RARE);
            final var unitFilter = model.getUnitFilter();
            if (unitFilter != null) {
                binding.unitInput.setText(unitFilter.getNameQuery());
                final var types = unitFilter.getTypes();
                if (types != null) {
                    final var unitTypeCheckboxes = getUnitTypesChips(binding);
                    for (final var cb : unitTypeCheckboxes) {
                        final var type = (UnitBaseData.Type) cb.getTag();
                        if (types.contains(type)) {
                            cb.setChecked(true);
                        }
                    }
                }
            }
            binding.atk.setTag(Combo.Type.ATTACK);
            binding.def.setTag(Combo.Type.DEFENSE);
            binding.spd.setTag(Combo.Type.MOVE_SPEED);
            binding.strong.setTag(Combo.Type.STRONG);
            binding.massive.setTag(Combo.Type.MASSIVE);
            binding.resist.setTag(Combo.Type.RESIST);
            binding.kb.setTag(Combo.Type.KNOCKBACK);
            binding.slow.setTag(Combo.Type.SLOW);
            binding.freeze.setTag(Combo.Type.FREEZE);
            binding.weaken.setTag(Combo.Type.WEAKEN);
            binding.strengthen.setTag(Combo.Type.STRENGTHEN);
            binding.crit.setTag(Combo.Type.CRITICAL);
            binding.startCannon.setTag(Combo.Type.CANNON_START);
            binding.cannonAtk.setTag(Combo.Type.CANNON_POWER);
            binding.recharge.setTag(Combo.Type.CANNON_RECHARGE);
            binding.baseDef.setTag(Combo.Type.BASE_DEFENSE);
            binding.startMoney.setTag(Combo.Type.STARTING_MONEY);
            binding.startLevel.setTag(Combo.Type.WORKER_START_LEVEL);
            binding.wallet.setTag(Combo.Type.WORKER_MAX);
            binding.research.setTag(Combo.Type.RESEARCH);
            binding.accounting.setTag(Combo.Type.ACCOUNTING);
            binding.study.setTag(Combo.Type.STUDY);
            binding.filterUnitConditionPicker.setAdapter(new ArrayAdapter<>(
                    requireContext(),
                    com.sixtyninefourtwenty.theming.R.layout.spinner_item_m3,
                    requireContext().getResources().getStringArray(R.array.filter_unit_condition_options)
            ));
            binding.filterUnitConditionPicker.setOnItemSelectedListener(simplify((parent, view, position, id) -> binding.filterUnitConditionExplanation.setText(switch (position) {
                case 0 -> getText(R.string.filter_combo_by_unit_condition_explanation_any);
                case 1 -> getText(R.string.filter_combo_by_unit_condition_explanation_all);
                default -> throw new AssertionError("Unrecognized position: " + position);
            })));
            final var comboFilter = model.getComboFilter();
            if (comboFilter != null) {
                final var unitFilterType = comboFilter.getUnitFilterType();
                if (unitFilterType != null) {
                    binding.filterUnitConditionPicker.setSelection(switch (unitFilterType) {
                        case ANY -> 0;
                        case ALL -> 1;
                    });
                }
                binding.nameInput.setText(comboFilter.getNameQuery());
                final var types = comboFilter.getTypes();
                if (types != null) {
                    final var comboCheckboxes = getComboTypesChips(binding);
                    for (final var cb : comboCheckboxes) {
                        final var type = (Combo.Type) cb.getTag();
                        if (types.contains(type)) {
                            cb.setChecked(true);
                        }
                    }
                }
            }
        }
    }

}
