package com.sixtyninefourtwenty.bcud.ui.fragments.favorites;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.repository.FavoritesDataRepository;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;

@NonNullTypesByDefault
@RequiredArgsConstructor
public final class FavoritesViewModel extends ViewModel {

    private final FavoritesDataRepository repository;

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> addFavorite(FavoriteItem item, Iterable<FavoriteReason> reasons) {
        return FluentFuture.from(repository.addFavorites(List.of(item)))
                .transformAsync(ignored -> repository.addFavoriteReasons(reasons), MoreExecutors.directExecutor());
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> addFavorites(Iterable<FavoriteItem> items, Iterable<FavoriteReason> reasons) {
        return FluentFuture.from(repository.addFavorites(items))
                .transformAsync(ignored -> repository.addFavoriteReasons(reasons), MoreExecutors.directExecutor());
    }

    public LiveData<Map<FavoriteItem, List<FavoriteReason>>> getAllFavoritesAndReasons() {
        return repository.getAllFavoritesAndReasons();
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> addFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return repository.addFavoriteReasons(reasons);
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> updateFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return repository.updateFavoriteReasons(reasons);
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> deleteFavoritesAndReasons(Iterable<FavoriteItem> items) {
        return repository.deleteFavorites(items);
    }

    @CanIgnoreReturnValue
    public ListenableFuture<@Nullable Void> deleteFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return repository.deleteFavoriteReasons(reasons);
    }

    public static final ViewModelInitializer<FavoritesViewModel> INITIALIZER = new ViewModelInitializer<>(FavoritesViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        final var myApp = (MyApplication) application;
        return new FavoritesViewModel(myApp.getFavoritesDataRepository());
    });

    public static FavoritesViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(FavoritesViewModel.class);
    }

}
