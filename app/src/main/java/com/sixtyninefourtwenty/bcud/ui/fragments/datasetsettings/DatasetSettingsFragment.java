package com.sixtyninefourtwenty.bcud.ui.fragments.datasetsettings;

import static java.util.Objects.requireNonNull;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.R;
import com.sixtyninefourtwenty.common.objects.UnitEEPriorityData;
import com.sixtyninefourtwenty.common.objects.UnitHypermaxData;
import com.sixtyninefourtwenty.common.objects.UnitTFMaterialData;
import com.sixtyninefourtwenty.common.objects.UnitTalentData;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import lombok.SneakyThrows;

public final class DatasetSettingsFragment extends PreferenceFragmentCompat {

    private DatasetSettingsViewModel viewModel;

    @SneakyThrows
    private void handleFilePickException(Exception e) {
        if (e instanceof IOException) {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.general_error)
                    .setMessage(Throwables.getStackTraceAsString(e))
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        } else if (e instanceof JSONException) {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.invalid_file)
                    .setMessage(Throwables.getStackTraceAsString(e))
                    .show();
        } else {
            throw e;
        }
    }

    private final ActivityResultLauncher<String[]> pickUnitTFMaterialData = registerForActivityResult(new ActivityResultContracts.OpenDocument(), uri -> {
        if (uri != null) {
            try (final var reader = new BufferedReader(new InputStreamReader(requireContext().getContentResolver().openInputStream(uri)))) {
                UnitTFMaterialData.SERIALIZER.listFromJson(new JSONArray(CharStreams.toString(reader)));
                viewModel.setUnitTFMaterialData(uri);
            } catch (Exception e) {
                handleFilePickException(e);
            }
        }
    });

    private final ActivityResultLauncher<String[]> pickUnitTalentData = registerForActivityResult(new ActivityResultContracts.OpenDocument(), uri -> {
        if (uri != null) {
            try (final var reader = new BufferedReader(new InputStreamReader(requireContext().getContentResolver().openInputStream(uri)))) {
                UnitTalentData.SERIALIZER.listFromJson(new JSONArray(CharStreams.toString(reader)));
                viewModel.setUnitTalentData(uri);
            } catch (Exception e) {
                handleFilePickException(e);
            }
        }
    });

    private final ActivityResultLauncher<String[]> pickUnitHPData = registerForActivityResult(new ActivityResultContracts.OpenDocument(), uri -> {
        if (uri != null) {
            try (final var reader = new BufferedReader(new InputStreamReader(requireContext().getContentResolver().openInputStream(uri)))) {
                UnitHypermaxData.SERIALIZER.listFromJson(new JSONArray(CharStreams.toString(reader)));
                viewModel.setUnitHPData(uri);
            } catch (Exception e) {
                handleFilePickException(e);
            }
        }
    });

    private final ActivityResultLauncher<String[]> pickUnitEEPData = registerForActivityResult(new ActivityResultContracts.OpenDocument(), uri -> {
        if (uri != null) {
            try (final var reader = new BufferedReader(new InputStreamReader(requireContext().getContentResolver().openInputStream(uri)))) {
                UnitEEPriorityData.SERIALIZER.listFromJson(new JSONArray(CharStreams.toString(reader)));
                viewModel.setUnitEEPData(uri);
            } catch (Exception e) {
                handleFilePickException(e);
            }
        }
    });

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        addPreferencesFromResource(R.xml.preferences_dataset);
        viewModel = DatasetSettingsViewModel.get(this);
        final var unitTFMaterialData = requireNonNull(this.<Preference>findPreference("unit_tf_material_data"));
        viewModel.getUnitTFMaterialDataLiveData().observe(this, uri -> unitTFMaterialData.setSummary(uri != null ? uri.toString() : getString(R.string.default1)));
        unitTFMaterialData.setOnPreferenceClickListener(preference -> {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(unitTFMaterialData.getTitle())
                    .setItems(new String[]{getString(R.string.default1), getString(R.string.custom)}, (dialog, which) -> {
                        switch (which) {
                            case 0 -> viewModel.setUnitTFMaterialData(null);
                            case 1 -> pickUnitTFMaterialData.launch(new String[]{"application/json"});
                        }
                    })
                    .setPositiveButton(android.R.string.cancel, null)
                    .show();
            return true;
        });
        final var unitTalentData = requireNonNull(this.<Preference>findPreference("unit_talent_data"));
        viewModel.getUnitTalentDataLiveData().observe(this, uri -> unitTalentData.setSummary(uri != null ? uri.toString() : getString(R.string.default1)));
        unitTalentData.setOnPreferenceClickListener(preference -> {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(unitTalentData.getTitle())
                    .setItems(new String[]{getString(R.string.default1), getString(R.string.custom)}, (dialog, which) -> {
                        switch (which) {
                            case 0 -> viewModel.setUnitTalentData(null);
                            case 1 -> pickUnitTalentData.launch(new String[]{"application/json"});
                        }
                    })
                    .setPositiveButton(android.R.string.cancel, null)
                    .show();
            return true;
        });
        final var unitHPData = requireNonNull(this.<Preference>findPreference("unit_hp_data"));
        viewModel.getUnitHPDataLiveData().observe(this, uri -> unitHPData.setSummary(uri != null ? uri.toString() : getString(R.string.default1)));
        unitHPData.setOnPreferenceClickListener(preference -> {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(unitHPData.getTitle())
                    .setItems(new String[]{getString(R.string.default1), getString(R.string.custom)}, (dialog, which) -> {
                        switch (which) {
                            case 0 -> viewModel.setUnitHPData(null);
                            case 1 -> pickUnitHPData.launch(new String[]{"application/json"});
                        }
                    })
                    .setPositiveButton(android.R.string.cancel, null)
                    .show();
            return true;
        });
        final var unitEEPData = requireNonNull(this.<Preference>findPreference("unit_eep_data"));
        viewModel.getUnitEEPDataLiveData().observe(this, uri -> unitEEPData.setSummary(uri != null ? uri.toString() : getString(R.string.default1)));
        unitEEPData.setOnPreferenceClickListener(preference -> {
            new MaterialAlertDialogBuilder(requireContext())
                    .setTitle(unitEEPData.getTitle())
                    .setItems(new String[]{getString(R.string.default1), getString(R.string.custom)}, (dialog, which) -> {
                        switch (which) {
                            case 0 -> viewModel.setUnitEEPData(null);
                            case 1 -> pickUnitEEPData.launch(new String[]{"application/json"});
                        }
                    })
                    .setPositiveButton(android.R.string.cancel, null)
                    .show();
            return true;
        });

        final var saveAndExit = requireNonNull(this.<Preference>findPreference("save_and_exit"));
        viewModel.getStateChanged().observe(this, saveAndExit::setVisible);
        saveAndExit.setOnPreferenceClickListener(preference -> {
            viewModel.persistData(requireContext().getContentResolver());
            MyApplication.get(requireContext()).getUnitData().refresh();
            NavHostFragment.findNavController(this).popBackStack();
            return true;
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (Boolean.TRUE.equals(viewModel.getStateChanged().getValue())) {
                    new MaterialAlertDialogBuilder(requireContext())
                            .setTitle(R.string.unsaved_changes)
                            .setMessage(R.string.save_changes_prompt)
                            .setPositiveButton(R.string.yes, (dialog, which) -> {
                                viewModel.persistData(requireContext().getContentResolver());
                                MyApplication.get(requireContext()).getUnitData().refresh();
                                NavHostFragment.findNavController(DatasetSettingsFragment.this).popBackStack();
                            })
                            .setNegativeButton(R.string.exit_without_saving, (dialog, which) -> {
                                NavHostFragment.findNavController(DatasetSettingsFragment.this).popBackStack();
                            })
                            .show();
                } else {
                    NavHostFragment.findNavController(DatasetSettingsFragment.this).popBackStack();
                }
            }
        });

        requireActivity().addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {

            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == android.R.id.home) {
                    requireActivity().getOnBackPressedDispatcher().onBackPressed();
                    return true;
                }
                return false;
            }
        }, this);
    }

}
