package com.sixtyninefourtwenty.bcud.ui.fragments.unitdesc.info;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.bcud.MyApplication;
import com.sixtyninefourtwenty.bcud.objects.Combo;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.repository.UnitData;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import lombok.Getter;

@NonNullTypesByDefault
public final class UnitDescUnitInfoViewModel extends ViewModel {

    private static final String CURRENT_UNIT_TO_DISPLAY_KEY = "current_unit_to_display";

    public UnitDescUnitInfoViewModel(SavedStateHandle savedStateHandle, UnitData data) {
        this.savedStateHandle = savedStateHandle;
        this.data = data;
        currentUnitToDisplay = savedStateHandle.getLiveData(CURRENT_UNIT_TO_DISPLAY_KEY);
    }

    private final SavedStateHandle savedStateHandle;
    private final UnitData data;
    @Getter
    private final LiveData<Unit> currentUnitToDisplay;

    public void setCurrentUnitToDisplay(Unit unit) {
        if (!unit.equals(currentUnitToDisplay.getValue())) {
            savedStateHandle.set(CURRENT_UNIT_TO_DISPLAY_KEY, unit);
        }
    }

    public ImmutableList<Combo> findCombosContainingUnit(Unit unit) {
        return data.findCombosContainingUnit(unit);
    }

    public static final ViewModelInitializer<UnitDescUnitInfoViewModel> INITIALIZER = new ViewModelInitializer<>(UnitDescUnitInfoViewModel.class, creationExtras -> {
        final var application = creationExtras.get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY);
        assert application != null;
        return new UnitDescUnitInfoViewModel(SavedStateHandleSupport.createSavedStateHandle(creationExtras), ((MyApplication) application).getUnitData());
    });

    public static UnitDescUnitInfoViewModel get(ViewModelStoreOwner owner) {
        return new ViewModelProvider(owner, ViewModelProvider.Factory.from(INITIALIZER)).get(UnitDescUnitInfoViewModel.class);
    }

}
