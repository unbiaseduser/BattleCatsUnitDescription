package com.sixtyninefourtwenty.bcud.repository;

import androidx.lifecycle.LiveData;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteItem;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoriteReason;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoritesDao;
import com.sixtyninefourtwenty.bcud.objects.favorites.FavoritesDatabase;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.utils.MoreExecutors2;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.List;
import java.util.Map;

@NonNullTypesByDefault
public final class FavoritesDataRepository {

    private final FavoritesDao dao;
    private final ListeningExecutorService createUpdateDeleteExecutor;

    public FavoritesDataRepository(
            FavoritesDatabase db,
            ListeningExecutorService createUpdateDeleteExecutor
    ) {
        this(db.favoritesDao(), createUpdateDeleteExecutor);
    }

    public FavoritesDataRepository(
            FavoritesDao dao,
            ListeningExecutorService createUpdateDeleteExecutor
    ) {
        this.dao = dao;
        this.createUpdateDeleteExecutor = createUpdateDeleteExecutor;
    }

    public LiveData<Map<FavoriteItem, List<FavoriteReason>>> getAllFavoritesAndReasons() {
        return dao.getAllFavoritesAndReasons();
    }

    public ListenableFuture<Map<FavoriteItem, List<FavoriteReason>>> getAllFavoritesAndReasonsSnapshot() {
        return dao.getAllFavoritesAndReasonsSnapshot();
    }

    public ListenableFuture<FavoriteItem> findFavoriteByUnitId(int id) {
        return dao.findFavoriteByUnitId(id);
    }

    public ListenableFuture<List<FavoriteReason>> getReasonsForFavoriteSnapshot(int id) {
        return dao.getReasonsForFavoriteSnapshot(id);
    }

    public ListenableFuture<@Nullable Void> addFavorites(Iterable<FavoriteItem> items) {
        return MoreExecutors2.submit(createUpdateDeleteExecutor, () -> dao.addFavorites(items));
    }

    public ListenableFuture<@Nullable Void> addFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return MoreExecutors2.submit(createUpdateDeleteExecutor, () -> dao.addFavoriteReasons(reasons));
    }

    public ListenableFuture<@Nullable Void> updateFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return MoreExecutors2.submit(createUpdateDeleteExecutor, () -> dao.updateFavoriteReasons(reasons));
    }

    public ListenableFuture<@Nullable Void> deleteFavorites(Iterable<FavoriteItem> items) {
        return MoreExecutors2.submit(createUpdateDeleteExecutor, () -> dao.deleteFavorites(items));
    }

    public ListenableFuture<@Nullable Void> deleteFavoriteReasons(Iterable<FavoriteReason> reasons) {
        return MoreExecutors2.submit(createUpdateDeleteExecutor, () -> dao.deleteFavoriteReasons(reasons));
    }

}
