package com.sixtyninefourtwenty.bcud.repository.helper;

import android.content.Context;
import android.widget.Toast;

import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.sixtyninefourtwenty.bcud.utils.DatasetPreferences;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.utils.Assets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.LazyThreadSafetyMode;

@NonNullTypesByDefault
public final class UnitTalentDataSupplierSupplier extends CommonDataSetSupplier<UnitTalentDataSupplier> implements Supplier<UnitTalentDataSupplier> {

    private final Lazy<UnitTalentDataSupplier> defaultSupplier = LazyKt.lazy(LazyThreadSafetyMode.NONE, () -> new UnitTalentDataParserCSV(Assets.openQuietly(context.getAssets(), "text/tp_data.txt")));

    public UnitTalentDataSupplierSupplier(Context context, DatasetPreferences preferences) {
        super(context, preferences);
    }

    @Override
    public UnitTalentDataSupplier get() {
        return get(
                preferences.getUnitTalentData(),
                defaultSupplier::getValue,
                uri -> {
                    try (final var reader = new BufferedReader(new InputStreamReader(context.getContentResolver().openInputStream(uri)))) {
                        return new UnitTalentDataParser(CharStreams.toString(reader));
                    } catch (IOException e) {
                        Toast.makeText(context, Throwables.getStackTraceAsString(e), Toast.LENGTH_SHORT).show();
                        return defaultSupplier.getValue();
                    }
                }
        );
    }
}
