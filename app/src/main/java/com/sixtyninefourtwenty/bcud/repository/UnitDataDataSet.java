package com.sixtyninefourtwenty.bcud.repository;

import android.content.res.AssetManager;

import com.sixtyninefourtwenty.bcud.repository.helper.ComboParser;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitEEPriorityReasoningSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitHPDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitParser;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTFMaterialDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTalentDataSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.repository.TalentSupplier;
import com.sixtyninefourtwenty.common.utils.Assets;

import java.util.function.Supplier;

@NonNullTypesByDefault
public final class UnitDataDataSet extends UnitDataDataSetCommon {

    public UnitDataDataSet(
            ComboParser comboParser,
            TalentSupplier talentSupplier,
            AssetManager assetManager,
            Supplier<UnitEEPriorityReasoningSupplier> unitEEPriorityReasoningSupplierSupplier,
            Supplier<UnitHPDataSupplier> unitHPDataSupplierSupplier,
            Supplier<UnitTalentDataSupplier> unitTalentDataSupplierSupplier,
            Supplier<UnitTFMaterialDataSupplier> unitTFMaterialDataSupplierSupplier
    ) {
        super(comboParser, talentSupplier, new UnitParser(Assets.readEntireTextFile(assetManager, "text/unit_base_data.json")), unitEEPriorityReasoningSupplierSupplier, unitHPDataSupplierSupplier, unitTalentDataSupplierSupplier, unitTFMaterialDataSupplierSupplier);
    }

}
