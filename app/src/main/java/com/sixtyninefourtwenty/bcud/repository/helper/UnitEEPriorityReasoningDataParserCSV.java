package com.sixtyninefourtwenty.bcud.repository.helper;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

import android.content.res.AssetManager;

import com.google.common.collect.ImmutableList;
import com.konloch.util.FastStringUtils;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.ElderEpic;
import com.sixtyninefourtwenty.common.utils.Assets;
import com.sixtyninefourtwenty.common.utils.CommonConstants;
import com.sixtyninefourtwenty.common.utils.ImmutableListCollector;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

@AllArgsConstructor
@NonNullTypesByDefault
public final class UnitEEPriorityReasoningDataParserCSV implements UnitEEPriorityReasoningSupplier {

    private final Map<Integer, Unit.ElderEpicTFPriorityReasoningTexts> data;

    @SneakyThrows
    public UnitEEPriorityReasoningDataParserCSV(InputStream input) {
        try (final var reader = new BufferedReader(new InputStreamReader(input))) {
            data = reader.lines()
                    .map(line -> FastStringUtils.split(line, CommonConstants.CSV_DELIMITER_PIPE))
                    .collect(toMap(
                            parts -> Integer.parseInt(parts[0]),
                            parts -> switch (ElderEpic.findByNumberInCSV(Integer.parseInt(parts[1]))) {
                                case ELDER -> Unit.ElderEpicTFPriorityReasoningTexts.of(
                                        parts[2],
                                        null
                                );
                                case EPIC -> Unit.ElderEpicTFPriorityReasoningTexts.of(
                                        null,
                                        parts[2]
                                );
                            },
                            /*
                            Assuming that elder/epic is distinct for each line associated with 1 unit id.
                            This is correct:
                            152|0|foo
                            152|1|bar
                            This is not:
                            152|0|foo
                            152|1|baz
                            152|0|bar (duplicate elder)

                            If there are duplicates, only the first line is accepted, all others are discarded.
                            */
                            (oldTexts, newTexts) -> {
                                final String elder;
                                final var oldElderText = oldTexts.getElder();
                                elder = oldElderText != null ? oldElderText : newTexts.getElder();
                                final String epic;
                                final var oldEpicText = oldTexts.getEpic();
                                epic = oldEpicText != null ? oldEpicText : newTexts.getEpic();
                                return Unit.ElderEpicTFPriorityReasoningTexts.of(elder, epic);
                            }
                    ));
        }
    }

    public UnitEEPriorityReasoningDataParserCSV(AssetManager assets) {
        this(Assets.openQuietly(assets, "text/eep_data.txt"));
    }

    @Nullable
    @Override
    public String getPriorityReasoningForUnitWithId(int unitId, ElderEpic elderEpic) {
        final var texts = requireNonNull(data.getOrDefault(unitId, Unit.ElderEpicTFPriorityReasoningTexts.EMPTY));
        return texts.getText(elderEpic);
    }

    @Override
    public ImmutableList<Unit> createElderEpicPriorityList(ElderEpic elderEpic, ImmutableList<Unit> unitsToSearchFrom) {
        return unitsToSearchFrom.stream()
                .filter(unit -> getPriorityReasoningForUnitWithId(unit.getId(), elderEpic) != null)
                .collect(new ImmutableListCollector<>());
    }

}
