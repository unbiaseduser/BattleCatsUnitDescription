package com.sixtyninefourtwenty.bcud.repository;

import android.content.res.AssetManager;

import com.sixtyninefourtwenty.bcud.repository.helper.ComboParser;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitEEPriorityReasoningSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitHPDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitParserCSV;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTFMaterialDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTalentDataSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.repository.TalentSupplier;
import com.sixtyninefourtwenty.common.utils.Assets;

import java.util.function.Supplier;

@NonNullTypesByDefault
public final class UnitDataDataSetCSV extends UnitDataDataSetCommon {

    public UnitDataDataSetCSV(
            ComboParser comboParser,
            TalentSupplier talentSupplier,
            AssetManager assetManager,
            Supplier<UnitEEPriorityReasoningSupplier> unitEEPriorityReasoningSupplierSupplier,
            Supplier<UnitHPDataSupplier> unitHPDataSupplierSupplier,
            Supplier<UnitTalentDataSupplier> unitTalentDataSupplierSupplier,
            Supplier<UnitTFMaterialDataSupplier> unitTFMaterialDataSupplierSupplier
    ) {
        super(comboParser, talentSupplier, new UnitParserCSV(Assets.openQuietly(assetManager, "text/unit_data.txt")), unitEEPriorityReasoningSupplierSupplier, unitHPDataSupplierSupplier, unitTalentDataSupplierSupplier, unitTFMaterialDataSupplierSupplier);
    }

}
