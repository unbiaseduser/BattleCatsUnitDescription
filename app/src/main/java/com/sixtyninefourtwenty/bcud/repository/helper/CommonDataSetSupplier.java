package com.sixtyninefourtwenty.bcud.repository.helper;

import android.content.Context;
import android.net.Uri;

import com.sixtyninefourtwenty.bcud.utils.DatasetPreferences;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import lombok.RequiredArgsConstructor;

@NonNullTypesByDefault
@RequiredArgsConstructor
public class CommonDataSetSupplier<T> {

    protected final Context context;
    protected final DatasetPreferences preferences;

    @Nullable
    protected Uri savedUri;

    @Nullable
    protected T savedSupplier;

    protected T get(
            @Nullable Uri savedPreference,
            Supplier<? extends T> defaultSupplier,
            Function<? super Uri, ? extends T> supplierCreation
    ) {
        if (savedPreference == null) {
            savedUri = null;
            savedSupplier = null;
            return defaultSupplier.get();
        }

        if (Objects.equals(savedPreference, savedUri)) {
            return Objects.requireNonNull(savedSupplier);
        }

        final var s = supplierCreation.apply(savedPreference);
        savedSupplier = s;
        savedUri = savedPreference;
        return s;
    }

}
