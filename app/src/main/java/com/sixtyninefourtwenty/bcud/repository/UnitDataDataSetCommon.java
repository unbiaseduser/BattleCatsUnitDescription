package com.sixtyninefourtwenty.bcud.repository;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.bcud.objects.Combo;
import com.sixtyninefourtwenty.bcud.objects.Unit;
import com.sixtyninefourtwenty.bcud.repository.helper.ComboParser;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitEEPriorityReasoningSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitHPDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTFMaterialDataSupplier;
import com.sixtyninefourtwenty.bcud.repository.helper.UnitTalentDataSupplier;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.objects.ElderEpic;
import com.sixtyninefourtwenty.common.objects.Hypermax;
import com.sixtyninefourtwenty.common.objects.Talent;
import com.sixtyninefourtwenty.common.objects.TalentData;
import com.sixtyninefourtwenty.common.objects.UnitBaseData;
import com.sixtyninefourtwenty.common.objects.repository.TalentSupplier;

import java.util.function.Function;
import java.util.function.Supplier;

import lombok.Getter;

@NonNullTypesByDefault
public class UnitDataDataSetCommon implements VerboseUnitData {

    private final ComboParser comboParser;
    private final TalentSupplier talentSupplier;
    private final UnitSupplier unitSupplier;
    private final Supplier<UnitEEPriorityReasoningSupplier> unitEEPriorityReasoningSupplierSupplier;
    private final Supplier<UnitHPDataSupplier> unitHPDataSupplierSupplier;
    private final Supplier<UnitTalentDataSupplier> unitTalentDataSupplierSupplier;
    private final Supplier<UnitTFMaterialDataSupplier> unitTFMaterialDataSupplierSupplier;

    protected UnitDataDataSetCommon(
            ComboParser comboParser,
            TalentSupplier talentSupplier,
            UnitSupplier unitSupplier,
            Supplier<UnitEEPriorityReasoningSupplier> unitEEPriorityReasoningSupplierSupplier,
            Supplier<UnitHPDataSupplier> unitHPDataSupplierSupplier,
            Supplier<UnitTalentDataSupplier> unitTalentDataSupplierSupplier,
            Supplier<UnitTFMaterialDataSupplier> unitTFMaterialDataSupplierSupplier
    ) {
        this.comboParser = comboParser;
        this.talentSupplier = talentSupplier;
        this.unitSupplier = unitSupplier;
        this.unitEEPriorityReasoningSupplierSupplier = unitEEPriorityReasoningSupplierSupplier;
        this.unitHPDataSupplierSupplier = unitHPDataSupplierSupplier;
        this.unitTalentDataSupplierSupplier = unitTalentDataSupplierSupplier;
        this.unitTFMaterialDataSupplierSupplier = unitTFMaterialDataSupplierSupplier;

        refresh();
    }

    @Override
    public void refresh() {
        final var unitEEPriorityReasoningSupplier = unitEEPriorityReasoningSupplierSupplier.get();
        final var unitHPDataSupplier = unitHPDataSupplierSupplier.get();
        final var unitTalentDataSupplier = unitTalentDataSupplierSupplier.get();
        final var unitTFMaterialDataSupplier = unitTFMaterialDataSupplierSupplier.get();
        final Function<TalentData, Talent> talentDataToTalentFunction = data -> data.getTalent(talentSupplier);

        storyLegends = unitSupplier.createMainList(UnitBaseData.Type.STORY_LEGEND,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        rares = unitSupplier.createMainList(UnitBaseData.Type.RARE,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        superRares = unitSupplier.createMainList(UnitBaseData.Type.SUPER_RARE,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        legendRares = unitSupplier.createMainList(UnitBaseData.Type.LEGEND_RARE,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        adventDrops = unitSupplier.createMainList(UnitBaseData.Type.ADVENT_DROP,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        cfSpecials = unitSupplier.createMainList(UnitBaseData.Type.CF_SPECIAL,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        ubers = unitSupplier.createMainList(UnitBaseData.Type.UBER,
                unitTFMaterialDataSupplier::getMaterialListForUnitWithId,
                unitTalentDataSupplier::getTalentListForUnitWithId,
                talentDataToTalentFunction);
        allUnits = new ImmutableList.Builder<Unit>()
                .addAll(storyLegends)
                .addAll(cfSpecials)
                .addAll(adventDrops)
                .addAll(rares)
                .addAll(superRares)
                .addAll(ubers)
                .addAll(legendRares)
                .build();
        specialMaxPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MAX, Hypermax.UnitType.SPECIAL, allUnits);
        specialHighPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.HIGH, Hypermax.UnitType.SPECIAL, allUnits);
        specialMidPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MID, Hypermax.UnitType.SPECIAL, allUnits);
        specialLowPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.LOW, Hypermax.UnitType.SPECIAL, allUnits);
        specialMinPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MIN, Hypermax.UnitType.SPECIAL, allUnits);
        rareMaxPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MAX, Hypermax.UnitType.RARE, allUnits);
        rareHighPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.HIGH, Hypermax.UnitType.RARE, allUnits);
        rareMidPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MID, Hypermax.UnitType.RARE, allUnits);
        rareLowPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.LOW, Hypermax.UnitType.RARE, allUnits);
        rareMinPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MIN, Hypermax.UnitType.RARE, allUnits);
        superRareMaxPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MAX, Hypermax.UnitType.SUPER_RARE, allUnits);
        superRareHighPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.HIGH, Hypermax.UnitType.SUPER_RARE, allUnits);
        superRareMidPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MID, Hypermax.UnitType.SUPER_RARE, allUnits);
        superRareLowPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.LOW, Hypermax.UnitType.SUPER_RARE, allUnits);
        superRareMinPriority = unitHPDataSupplier.createHypermaxPriorityList(Hypermax.Priority.MIN, Hypermax.UnitType.SUPER_RARE, allUnits);
        nonUberTopPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.TOP, Talent.UnitType.NON_UBER, allUnits);
        nonUberHighPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.HIGH, Talent.UnitType.NON_UBER, allUnits);
        nonUberMidPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.MID, Talent.UnitType.NON_UBER, allUnits);
        nonUberLowPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.LOW, Talent.UnitType.NON_UBER, allUnits);
        nonUberDoNotUnlock = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.DONT, Talent.UnitType.NON_UBER, allUnits);
        uberTopPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.TOP, Talent.UnitType.UBER, allUnits);
        uberHighPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.HIGH, Talent.UnitType.UBER, allUnits);
        uberMidPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.MID, Talent.UnitType.UBER, allUnits);
        uberLowPriority = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.LOW, Talent.UnitType.UBER, allUnits);
        uberDoNotUnlock = unitTalentDataSupplier.createTalentPriorityList(Talent.Priority.DONT, Talent.UnitType.UBER, allUnits);
        elderList = unitEEPriorityReasoningSupplier.createElderEpicPriorityList(ElderEpic.ELDER, allUnits);
        epicList = unitEEPriorityReasoningSupplier.createElderEpicPriorityList(ElderEpic.EPIC, allUnits);
        allCombos = comboParser.createCombos(allUnits);
    }

    @Getter
    private ImmutableList<Unit> storyLegends;
    @Getter
    private ImmutableList<Unit> rares;
    @Getter
    private ImmutableList<Unit> superRares;
    @Getter
    private ImmutableList<Unit> legendRares;
    @Getter
    private ImmutableList<Unit> adventDrops;
    @Getter
    private ImmutableList<Unit> cfSpecials;
    @Getter
    private ImmutableList<Unit> ubers;
    @Getter
    private ImmutableList<Unit> allUnits;
    @Getter
    private ImmutableList<Unit> specialMaxPriority;
    @Getter
    private ImmutableList<Unit> specialHighPriority;
    @Getter
    private ImmutableList<Unit> specialMidPriority;
    @Getter
    private ImmutableList<Unit> specialLowPriority;
    @Getter
    private ImmutableList<Unit> specialMinPriority;
    @Getter
    private ImmutableList<Unit> rareMaxPriority;
    @Getter
    private ImmutableList<Unit> rareHighPriority;
    @Getter
    private ImmutableList<Unit> rareMidPriority;
    @Getter
    private ImmutableList<Unit> rareLowPriority;
    @Getter
    private ImmutableList<Unit> rareMinPriority;
    @Getter
    private ImmutableList<Unit> superRareMaxPriority;
    @Getter
    private ImmutableList<Unit> superRareHighPriority;
    @Getter
    private ImmutableList<Unit> superRareMidPriority;
    @Getter
    private ImmutableList<Unit> superRareLowPriority;
    @Getter
    private ImmutableList<Unit> superRareMinPriority;
    @Getter
    private ImmutableList<Unit> nonUberTopPriority;
    @Getter
    private ImmutableList<Unit> nonUberHighPriority;
    @Getter
    private ImmutableList<Unit> nonUberMidPriority;
    @Getter
    private ImmutableList<Unit> nonUberLowPriority;
    @Getter
    private ImmutableList<Unit> nonUberDoNotUnlock;
    @Getter
    private ImmutableList<Unit> uberTopPriority;
    @Getter
    private ImmutableList<Unit> uberHighPriority;
    @Getter
    private ImmutableList<Unit> uberMidPriority;
    @Getter
    private ImmutableList<Unit> uberLowPriority;
    @Getter
    private ImmutableList<Unit> uberDoNotUnlock;
    @Getter
    private ImmutableList<Unit> elderList;
    @Getter
    private ImmutableList<Unit> epicList;
    @Getter
    private ImmutableList<Combo> allCombos;

}
