package com.sixtyninefourtwenty.bcud.repository.helper;

import android.content.Context;
import android.widget.Toast;

import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.sixtyninefourtwenty.bcud.utils.DatasetPreferences;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.utils.Assets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.LazyThreadSafetyMode;

@NonNullTypesByDefault
public final class UnitHPDataSupplierSupplier extends CommonDataSetSupplier<UnitHPDataSupplier> implements Supplier<UnitHPDataSupplier> {

    private final Lazy<UnitHPDataSupplier> defaultSupplier = LazyKt.lazy(LazyThreadSafetyMode.NONE, () -> new UnitHPDataParserCSV(Assets.openQuietly(context.getAssets(), "text/hp_data.txt")));

    public UnitHPDataSupplierSupplier(Context context, DatasetPreferences preferences) {
        super(context, preferences);
    }

    @Override
    public UnitHPDataSupplier get() {
        return get(
                preferences.getUnitHPData(),
                defaultSupplier::getValue,
                uri -> {
                    try (final var reader = new BufferedReader(new InputStreamReader(context.getContentResolver().openInputStream(uri)))) {
                        return new UnitHPDataParser(CharStreams.toString(reader));
                    } catch (IOException e) {
                        Toast.makeText(context, Throwables.getStackTraceAsString(e), Toast.LENGTH_SHORT).show();
                        return defaultSupplier.getValue();
                    }
                }
        );
    }
}
