package com.sixtyninefourtwenty.bcud.repository.helper;

import android.content.Context;
import android.widget.Toast;

import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.sixtyninefourtwenty.bcud.utils.DatasetPreferences;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.utils.Assets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

import kotlin.Lazy;
import kotlin.LazyKt;
import kotlin.LazyThreadSafetyMode;

@NonNullTypesByDefault
public final class UnitEEPriorityReasoningSupplierSupplier extends CommonDataSetSupplier<UnitEEPriorityReasoningSupplier> implements Supplier<UnitEEPriorityReasoningSupplier> {

    private final Lazy<UnitEEPriorityReasoningSupplier> defaultSupplier = LazyKt.lazy(LazyThreadSafetyMode.NONE, () -> new UnitEEPriorityReasoningDataParserCSV(Assets.openQuietly(context.getAssets(), "text/eep_data.txt")));

    public UnitEEPriorityReasoningSupplierSupplier(Context context, DatasetPreferences preferences) {
        super(context, preferences);
    }

    @Override
    public UnitEEPriorityReasoningSupplier get() {
        return get(
                preferences.getUnitEEPData(),
                defaultSupplier::getValue,
                uri -> {
                    try (final var reader = new BufferedReader(new InputStreamReader(context.getContentResolver().openInputStream(uri)))) {
                        return new UnitEEPriorityReasoningDataParser(CharStreams.toString(reader));
                    } catch (IOException e) {
                        Toast.makeText(context, Throwables.getStackTraceAsString(e), Toast.LENGTH_SHORT).show();
                        return defaultSupplier.getValue();
                    }
                }
        );
    }
}
