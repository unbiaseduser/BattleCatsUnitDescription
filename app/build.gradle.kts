plugins {
    alias(libs.plugins.androidApplication)
    id("androidx.navigation.safeargs")
    alias(libs.plugins.androidJunit5)
    id("androidx.room")
}

room {
    schemaDirectory("$projectDir/schemas")
}

android {
    compileSdk = 35

    defaultConfig {
        applicationId = "com.sixtyninefourtwenty.bcud"
        minSdk = 21
        targetSdk = 35
        versionCode = 1
        versionName = "1.0"

        setProperty("archivesBaseName", "BCUD v$versionName")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunnerArguments["runnerBuilder"] = "de.mannodermaus.junit5.AndroidJUnit5Builder"

        javaCompileOptions {
            annotationProcessorOptions {
                arguments += mapOf(
                    "room.incremental" to "true" //wait why did I add this?
                )
            }
        }
    }

    signingConfigs {
        create("release") {
            storeFile = findProperty("androidStoreFile")?.let { File(it.toString()) }
            keyAlias = findProperty("androidKeyAlias")?.toString()
            storePassword = findProperty("androidStorePassword")?.toString()
            keyPassword = findProperty("androidKeyPassword")?.toString()
        }
    }

    buildTypes {
        debug {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }

        release {
            signingConfig = findProperty("androidStoreFile")?.let { signingConfigs["release"] }
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles += getDefaultProguardFile("proguard-android-optimize.txt")
            proguardFiles += file("proguard-rules.pro")
        }
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }

    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    testOptions {
        unitTests.isIncludeAndroidResources = true
    }

    sourceSets {
        // Adds exported schema location as test app assets.
        getByName("androidTest").assets.srcDir("$projectDir/schemas")
    }


    namespace = "com.sixtyninefourtwenty.bcud"

}

dependencies {

    configurations.all {
        exclude(group = "com.github.unbiaseduser.custom-preferences", module = "colorpicker")
    }

    implementation(project(":common"))
    implementation(libs.core)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.recyclerview)
    implementation(libs.recyclerview.selection)
    coreLibraryDesugaring(libs.desugar.jdk.libs)
    implementation(libs.lifecycle.livedata.ktx)
    implementation(libs.lifecycle.viewmodel.ktx)
    implementation(libs.lifecycle.runtime.ktx)
    implementation(libs.navigation.fragment)
    implementation(libs.navigation.ui)
    implementation(libs.browser)
    implementation(libs.fastscroll)
    implementation(libs.commonmark)
    implementation(libs.commonmark.ext.gfm.strikethrough)
    implementation(libs.commonmark.ext.gfm.tables)
    configurations.all {
        exclude(group = "com.atlassian.commonmark") //exclude the old commonmark version pulled in by markwon to manually use the latest version
    }
    implementation (libs.markwon.core)
    implementation (libs.markwon.html)
    implementation (libs.markwon.image)
    implementation (libs.markwon.image.coil)
    implementation (libs.markwon.recycler)
    implementation (libs.markwon.recycler.table)

    implementation(libs.preference)
    implementation(libs.work.runtime)
    implementation(libs.fulldraggabledrawer)
    implementation(libs.photoeditor)
    implementation(libs.betterlinkmovementmethod)

    implementation(libs.room.runtime)
    implementation(libs.room.guava)
    annotationProcessor(libs.room.compiler)

    implementation(libs.guava)
    implementation(libs.fastutil)
    implementation(libs.coil)
    implementation(libs.balloon)
    implementation(libs.durationhumanizer)
    implementation(libs.swipetoactionlayout)
    implementation(libs.expandablelayout)
    implementation(libs.multistateview)
    implementation(libs.switchicon)
    implementation(libs.circularimageview)
    implementation(libs.flowlayout)
    implementation(libs.vavr)
    implementation(libs.faststringutils)
    implementation(libs.materialaboutlibrary)
    implementation(libs.custompreferencesthemingintegration)
    implementation(libs.bottomsheetalertdialog)

    compileOnly(libs.lombok)
    annotationProcessor(libs.lombok)

    testImplementation(libs.json)
    testImplementation(libs.androidx.test.ext.junit)
    testImplementation(libs.junit)
    testRuntimeOnly(libs.junit.vintage.engine)
    testImplementation(libs.junit.jupiter.api)
    testRuntimeOnly(libs.junit.jupiter.engine)
    testImplementation(libs.robolectric)
    androidTestImplementation(libs.androidx.room.testing)
    androidTestImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(libs.espresso.contrib)
    androidTestImplementation(libs.androidx.core.testing)
    androidTestImplementation(libs.awaitility)
    androidTestImplementation(libs.junit.jupiter.api)
    androidTestRuntimeOnly(libs.junit.jupiter.engine)
    androidTestImplementation(libs.android.junit5.test.core)
    androidTestRuntimeOnly(libs.android.junit5.test.runner)
    debugImplementation(libs.fragment.testing)

}