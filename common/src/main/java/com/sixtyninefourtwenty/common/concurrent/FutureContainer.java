package com.sixtyninefourtwenty.common.concurrent;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;

public class FutureContainer {

    protected final Set<Future<?>> futures = new HashSet<>();

    public void add(Future<?> future) {
        futures.add(future);
    }

    public <T extends Future<?>> T addAndReturn(T future) {
        add(future);
        return future;
    }

    public void close() {
        for (final var f : futures) {
            f.cancel(false);
        }
        futures.clear();
    }

}
