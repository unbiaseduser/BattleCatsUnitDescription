package com.sixtyninefourtwenty.common.concurrent;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

public final class LifecycleAwareFutureContainer extends FutureContainer implements LifecycleEventObserver {

    private final Lifecycle lifecycle;

    public LifecycleAwareFutureContainer(Lifecycle lifecycle) {
        this.lifecycle = lifecycle;
        final var state = lifecycle.getCurrentState();
        if (state != Lifecycle.State.DESTROYED && state.isAtLeast(Lifecycle.State.INITIALIZED)) {
            lifecycle.addObserver(this);
        }
    }

    public LifecycleAwareFutureContainer(LifecycleOwner lifecycleOwner) {
        this(lifecycleOwner.getLifecycle());
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner lifecycleOwner, @NonNull Lifecycle.Event event) {
        if (lifecycle.getCurrentState().compareTo(Lifecycle.State.DESTROYED) <= 0) {
            lifecycle.removeObserver(this);
            close();
        }
    }
}
