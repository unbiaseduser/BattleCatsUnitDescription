package com.sixtyninefourtwenty.common.concurrent;

import androidx.lifecycle.ViewModel;

import java.io.Closeable;

public final class ViewModelFutureContainer extends FutureContainer implements Closeable {

    public ViewModelFutureContainer(ViewModel viewModel) {
        viewModel.addCloseable(this);
    }

}
