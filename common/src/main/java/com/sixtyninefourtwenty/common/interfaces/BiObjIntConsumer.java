package com.sixtyninefourtwenty.common.interfaces;

@FunctionalInterface
public interface BiObjIntConsumer<T, U> {

    void accept(T t, U u, int value);

}
