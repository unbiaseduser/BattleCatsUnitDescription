package com.sixtyninefourtwenty.common.interfaces;

import android.view.MenuItem;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface MyMenuItemClickListener extends MenuItem.OnMenuItemClickListener {

    @Override
    default boolean onMenuItemClick(@NonNull MenuItem item) {
        onMenuItemClickCustom(item);
        return true;
    }

    void onMenuItemClickCustom(@NonNull MenuItem item);

    static MenuItem.OnMenuItemClickListener voidReturning(MyMenuItemClickListener listener) {
        return listener;
    }

}
