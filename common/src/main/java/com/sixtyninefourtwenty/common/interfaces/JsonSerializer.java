package com.sixtyninefourtwenty.common.interfaces;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lombok.SneakyThrows;

@NonNullTypesByDefault
public interface JsonSerializer<T> {

    JSONObject toJson(T obj) throws JSONException;

    T fromJson(JSONObject obj) throws JSONException;

    @SneakyThrows
    default JSONArray listToJson(List<T> list) {
        final var array = new JSONArray();
        for (final var elem : list) {
            array.put(toJson(elem));
        }
        return array;
    }

    @SneakyThrows
    default List<T> listFromJson(JSONArray array) {
        final var arrLength = array.length();
        final var list = new ArrayList<T>(arrLength);
        for (int i = 0; i < arrLength; i++) {
            list.add(fromJson(array.getJSONObject(i)));
        }
        return list;
    }

}
