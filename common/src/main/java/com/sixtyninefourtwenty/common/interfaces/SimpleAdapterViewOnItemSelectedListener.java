package com.sixtyninefourtwenty.common.interfaces;

import android.widget.AdapterView;

@FunctionalInterface
public interface SimpleAdapterViewOnItemSelectedListener extends AdapterView.OnItemSelectedListener {

    @Override
    default void onNothingSelected(AdapterView<?> parent) {

    }

    static AdapterView.OnItemSelectedListener simplify(SimpleAdapterViewOnItemSelectedListener listener) {
        return listener;
    }

}
