package com.sixtyninefourtwenty.common.utils;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Function;
import java.util.stream.Stream;

import kotlin.io.TextStreamsKt;
import lombok.SneakyThrows;

@NonNullTypesByDefault
public final class Assets {

    private Assets() { throw new UnsupportedOperationException(); }

    @SneakyThrows
    @Nullable
    public static Bitmap readBitmap(AssetManager assetManager, String path) {
        return BitmapFactory.decodeStream(assetManager.open(path));
    }

    @SneakyThrows
    public static <R> R readTextFileLines(AssetManager assetManager, String path, Function<? super Stream<String>, ? extends R> block) {
        try (final var reader = new BufferedReader(new InputStreamReader(assetManager.open(path)))) {
            return block.apply(reader.lines());
        }
    }

    @SneakyThrows
    @Nullable
    public static String readFirstFileLine(AssetManager assetManager, String path) {
        try (final var reader = new BufferedReader(new InputStreamReader(assetManager.open(path)))) {
            return reader.readLine();
        }
    }

    @SneakyThrows
    public static InputStream openQuietly(AssetManager assetManager, String path) {
        return assetManager.open(path);
    }

    @SneakyThrows
    public static String readEntireTextFile(AssetManager assetManager, String path) {
        try (final var reader = new BufferedReader(new InputStreamReader(assetManager.open(path)))) {
            return TextStreamsKt.readText(reader);
        }
    }

    @SneakyThrows
    @Nullable
    public static Drawable createDrawableFromAsset(AssetManager assetManager, String path) {
        try (final var is = assetManager.open(path)) {
            return Drawable.createFromStream(is, path);
        }
    }

}
