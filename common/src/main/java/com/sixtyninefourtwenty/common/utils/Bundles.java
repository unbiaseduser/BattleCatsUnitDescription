package com.sixtyninefourtwenty.common.utils;

import android.os.Bundle;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import java.util.function.Consumer;

@NonNullTypesByDefault
public final class Bundles {

    private Bundles() { throw new UnsupportedOperationException(); }

    public static Bundle createBundle(Consumer<? super Bundle> block) {
        final var b = new Bundle();
        block.accept(b);
        return b;
    }

}
