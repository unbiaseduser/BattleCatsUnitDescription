package com.sixtyninefourtwenty.common.utils;

import android.content.res.Resources;
import android.os.Build;
import android.util.TypedValue;

import androidx.annotation.Dimension;
import androidx.annotation.Px;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

@NonNullTypesByDefault
public final class Dimensions {

    private Dimensions() { throw new UnsupportedOperationException(); }

    @Dimension(unit = Dimension.DP)
    public static float pxToDp(Resources resources, @Px float px) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            return TypedValue.deriveDimension(TypedValue.COMPLEX_UNIT_DIP, px, resources.getDisplayMetrics());
        }
        return px / resources.getDisplayMetrics().density;
    }

}
