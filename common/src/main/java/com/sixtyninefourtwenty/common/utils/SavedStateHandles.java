package com.sixtyninefourtwenty.common.utils;

import android.os.Parcelable;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

@NonNullTypesByDefault
public final class SavedStateHandles {

    private SavedStateHandles() { throw new UnsupportedOperationException(); }

    public static <T extends @Nullable Parcelable> MutableLiveData<T> getNullableParcelableLiveData(
            SavedStateHandle savedStateHandle,
            String key,
            T initialValue
    ) {
        return savedStateHandle.getLiveData(key, initialValue);
    }

}
