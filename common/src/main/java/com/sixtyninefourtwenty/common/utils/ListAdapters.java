package com.sixtyninefourtwenty.common.utils;

import androidx.recyclerview.widget.ListAdapter;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@NonNullTypesByDefault
public final class ListAdapters {

    private ListAdapters() { throw new UnsupportedOperationException(); }

    public static <T> List<T> copyOfCurrentList(ListAdapter<T, ?> adapter) {
        return new ArrayList<>(adapter.getCurrentList());
    }

    public static <T> void modifyList(ListAdapter<T, ?> adapter, Consumer<? super List<T>> block) {
        final var l = copyOfCurrentList(adapter);
        block.accept(l);
        adapter.submitList(l);
    }

    public static <T> void addElement(ListAdapter<T, ?> adapter, T element) {
        modifyList(adapter, l -> l.add(element));
    }

    public static <T> void replaceElement(ListAdapter<T, ?> adapter, T existingElement, T newElement) {
        modifyList(adapter, l -> l.replaceAll(elem -> elem.equals(existingElement) ? newElement : elem));
    }

    public static <T> void removeElement(ListAdapter<T, ?> adapter, T element) {
        modifyList(adapter, l -> l.remove(element));
    }

}
