package com.sixtyninefourtwenty.common.utils;

public final class CommonConstants {

    private CommonConstants() {}

    public static final String CSV_DELIMITER_COMMA = ",";

    public static final String CSV_DELIMITER_PIPE = "|";

}
