package com.sixtyninefourtwenty.common.utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import java.util.List;
import java.util.function.Supplier;

@NonNullTypesByDefault
public final class ListFragmentStateAdapter extends FragmentStateAdapter {

    private final List<Supplier<? extends Fragment>> fragmentSuppliers;

    public ListFragmentStateAdapter(
            FragmentActivity fragmentActivity,
            List<Supplier<? extends Fragment>> fragmentSuppliers
    ) {
        super(fragmentActivity);
        this.fragmentSuppliers = fragmentSuppliers;
    }

    @Override
    public Fragment createFragment(int position) {
        return fragmentSuppliers.get(position).get();
    }

    @Override
    public int getItemCount() {
        return fragmentSuppliers.size();
    }

}
