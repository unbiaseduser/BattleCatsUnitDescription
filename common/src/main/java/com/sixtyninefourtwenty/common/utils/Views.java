package com.sixtyninefourtwenty.common.utils;

import android.view.View;
import android.widget.EditText;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

@NonNullTypesByDefault
public final class Views {

    private Views() { throw new UnsupportedOperationException(); }

    public static void toggleVisibility(View view, int visibilityToToggleTo) {
        view.setVisibility(view.getVisibility() == View.VISIBLE ? visibilityToToggleTo : View.VISIBLE);
    }

    public static void toggleVisibility(View view) {
        toggleVisibility(view, View.GONE);
    }

    public static boolean isBlank(EditText editText) {
        final var t = editText.getText();
        return t == null || t.toString().isBlank();
    }

    public static String getInput(EditText editText) {
        final var t = editText.getText();
        if (t == null) {
            return "";
        }
        return t.toString().trim();
    }

}
