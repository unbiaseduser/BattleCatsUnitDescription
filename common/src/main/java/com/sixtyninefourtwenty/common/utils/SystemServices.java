package com.sixtyninefourtwenty.common.utils;

import android.app.usage.StorageStatsManager;
import android.app.usage.UsageStatsManager;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

@NonNullTypesByDefault
public final class SystemServices {

    private SystemServices() { throw new UnsupportedOperationException(); }

    public static ClipboardManager getClipboardManager(Context context) {
        return (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    public static UsageStatsManager getUsageStatsManager(Context context) {
        return (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static StorageStatsManager getStorageStatsManager(Context context) {
        return (StorageStatsManager) context.getSystemService(Context.STORAGE_STATS_SERVICE);
    }

}
