package com.sixtyninefourtwenty.common.utils;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

import org.checkerframework.checker.nullness.qual.Nullable;

@NonNullTypesByDefault
public final class MoreExecutors2 {

    public static ListenableFuture<@Nullable Void> submit(ListeningExecutorService executorService, Runnable runnable) {
        return executorService.submit(() -> {
            runnable.run();
            return null;
        });
    }

}
