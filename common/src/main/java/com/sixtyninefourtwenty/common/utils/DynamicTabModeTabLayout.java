package com.sixtyninefourtwenty.common.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

/**
 * @see <a href="https://stackoverflow.com/a/44894143">stackoverflow answer</a>
 */
public final class DynamicTabModeTabLayout extends TabLayout {

    public DynamicTabModeTabLayout(Context context) {
        super(context);
    }

    public DynamicTabModeTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicTabModeTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setTabMode(MODE_SCROLLABLE);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (getTabCount() == 0)
            return;

        final var tabLayout = (ViewGroup) getChildAt(0);
        int widthOfAllTabs = 0;
        for (int i = 0; i < tabLayout.getChildCount(); i++) {
            widthOfAllTabs += tabLayout.getChildAt(i).getMeasuredWidth();
        }
        if (widthOfAllTabs <= getMeasuredWidth()) {
            setTabMode(MODE_FIXED);
            // without this the tabs just get squished for some reason?
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

}
