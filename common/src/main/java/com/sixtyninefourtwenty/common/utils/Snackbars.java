package com.sixtyninefourtwenty.common.utils;

import android.view.View;

import androidx.annotation.StringRes;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;

@NonNullTypesByDefault
public final class Snackbars {

    private Snackbars() { throw new UnsupportedOperationException(); }

    public static Snackbar makeSnackbar(View view, @StringRes int textRes) {
        return Snackbar.make(view, textRes, BaseTransientBottomBar.LENGTH_SHORT);
    }

}
