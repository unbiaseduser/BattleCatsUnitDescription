package com.sixtyninefourtwenty.common.objects;

import androidx.annotation.StringRes;

import com.google.common.collect.ImmutableList;
import com.sixtyninefourtwenty.common.R;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ElderEpic {
    ELDER(R.string.elder, 0), EPIC(R.string.epic, 1);

    @StringRes
    private final int text;
    private final int numberInCSV;

    public static final ImmutableList<ElderEpic> VALUES = ImmutableList.copyOf(values());
    public static ElderEpic findByNumberInCSV(int numberInCSV) {
        return VALUES.stream()
                .filter(e -> e.numberInCSV == numberInCSV)
                .findFirst()
                .orElseThrow();
    }

}
