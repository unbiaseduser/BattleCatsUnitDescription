package com.sixtyninefourtwenty.common.objects;

import static java.util.Objects.requireNonNull;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.sixtyninefourtwenty.common.annotations.NonNullTypesByDefault;
import com.sixtyninefourtwenty.common.interfaces.JsonSerializer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;

@Value
@AllArgsConstructor
@NonNullTypesByDefault
public class UnitTalentData implements Parcelable {

    int unitId;
    Talent.UnitType unitType;
    @With
    List<TalentData> talents;

    public static final JsonSerializer<UnitTalentData> SERIALIZER = new JsonSerializer<>() {
        @Override
        @NonNull
        public JSONObject toJson(UnitTalentData obj) throws JSONException {
            return new JSONObject()
                    .put("unit_id", obj.unitId)
                    .put("unit_type", obj.unitType.name())
                    .put("talents", TalentData.SERIALIZER.listToJson(obj.talents));
        }

        @Override
        @NonNull
        public UnitTalentData fromJson(JSONObject obj) throws JSONException {
            return new UnitTalentData(
                    obj.getInt("unit_id"),
                    Talent.UnitType.valueOf(obj.getString("unit_type")),
                    TalentData.SERIALIZER.listFromJson(obj.getJSONArray("talents"))
            );
        }
    };

    private UnitTalentData(Parcel in) {
        unitId = in.readInt();
        unitType = (Talent.UnitType) requireNonNull(in.readSerializable());
        talents = requireNonNull(in.createTypedArrayList(TalentData.CREATOR));
    }

    public static final Creator<UnitTalentData> CREATOR = new Creator<>() {
        @Override
        public UnitTalentData createFromParcel(Parcel in) {
            return new UnitTalentData(in);
        }

        @Override
        public UnitTalentData[] newArray(int size) {
            return new UnitTalentData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(unitId);
        dest.writeSerializable(unitType);
        dest.writeTypedList(talents);
    }
}
